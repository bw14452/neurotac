# encoding: utf-8
import os, pickle
import numpy as np
import pyNN.nest as sim
from pyNN.utility import get_simulator, init_logging, normalized_filename
from pyNN.parameters import Sequence
from pyNN.random import RandomDistribution as rnd, NumpyRNG

############################ PARAMETERS #################################

n = 20     # Number of cells
w = 0.002  # synaptic weight (µS)
cell_parameters = {
    "tau_m": 10.0,       # cell membrane time constant -- (ms)
    "tau_refrac": 1.0,    # refractory period duration -- (ms)
    "cm": 1.0,           # membrane capacity -- (nF)
    "tau_syn_E": 1.0,      # rise time of the excitatory synaptic function -- (ms)
    "tau_syn_I": 1.0,      # rise time of the inhibitory synaptic function -- (ms)
    "i_offset": 0.0,     # Offset current.
    "v_thresh": -50.0,   # spike threshold -- (mV)
    "v_reset": -60.0,    # reset potential post-spike -- (mV)
    "v_rest": -70.0,     # resting membrane potential -- (mV)
}
neuron_type = sim.IF_curr_exp(**cell_parameters)
dt         = 0.1           # (ms)
syn_delay  = 1.0           # (ms)
input_rate = 50.0          # (Hz)
simtime    = 1000          # (ms)
sim.setup(timestep=1.0, min_delay=syn_delay, max_delay=syn_delay)

######################### BUILD NETWORK #############################

# # Layer 1
# FirstLayerStructure = sim.space.Grid2D(aspect_ratio  = 1, dx = 1.0, dy = 1.0, fill_order = 'sequential')  
# FirstLayerStructure.generate_positions(no_of_pixels) 
# FirstLayer = sim.Population(no_of_pixels, neuron_type, structure = FirstLayerStructure, label='First_Layer')     

######################### LOAD DATA #############################
dataset_folder = 'collect_taps_orientation_07081755'

# Loop over poses/trials
pose_idx = 0
trial_idx = 0
# for pos in range (NumberOfPoses):
#     for trial in range (NumberOfTrials):
dataset_filename = 'taps_orientation_trial_' +str(trial_idx) +'_pose_'  + str(pose_idx) + '.pickle'
dataset_path = os.path.join(os.environ['DATAPATH'], 'NeuroTac_DAVIS240', 'ABB_edge_orientation', dataset_folder,dataset_filename)
data_in = open(dataset_path, "rb") 
loaded_data = pickle.load(data_in)
pre_data = [None] * loaded_data.size
pre_data = loaded_data.flatten('C')                 # -- Load and flatten data to 1D array
no_of_pixels = pre_data.size
simtime = max(max(pre_data))
for x in range(0,no_of_pixels):
    pre_data[x] = Remove(pre_data[x])               # -- Remove duplicate timestamps
for i in range(len(pre_data)):                      # -- Reorder arrays
    pre_data[i] = np.sort(pre_data[i])
inputarray = Sequencer(pre_data, no_of_pixels)      # -- Convert 1D array to a pyNN Sequence class


# Input layer
layer_input = sim.Population(no_of_pixels, sim.SpikeSourceArray (spike_times = inputarray), structure = FirstLayerStructure, label = 'InputLayer')                                            
layer_input.record('spikes')

# === Run simulation ===========================================================
sim.run(simtime)

filename = normalized_filename("Results", "small_network", "pkl",
                               "simulator", sim.num_processes())
cells.write_data(filename, annotations={'script_name': __file__})

print("Mean firing rate: ", cells.mean_spike_count() * 1000.0 / simtime, "Hz")


# === Clean up and quit ========================================================

sim.end()