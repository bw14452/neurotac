"""
__author__ = Nicolas Perez-Nieves
__email__ = nicolas.perez14@imperial.ac.uk

SDNN Implementation based on Kheradpisheh, S.R., et al. 'STDP-based spiking deep neural networks 
for object recognition'. arXiv:1611.01421v1 (Nov, 2016)
"""

from SDNN_cuda import SDNN
from Classifier import Classifier
import numpy as np
import os, json, pickle
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from collections import OrderedDict
from os.path import dirname, realpath
from math import floor

import time


# ------------------------------- Data loading and converting functions-------------------------------#

def remove_duplicates(data):
    for i in range(len(data)):
        for j in range(len(data[i])): 
            data[i][j] = list(OrderedDict.fromkeys(data[i][j]))
    return data

def count_spikes(data):
    for i in range(len(data)):
        for j in range(len(data[i])):
            data[i][j] = len(data[i][j])
    return data

def first_spikes(data):
    for i in range(len(data)):
        for j in range(len(data[i])):
            if len(data[i][j])>0:
                data[i][j] = min(data[i][j])
            else:
                data[i][j] = 0
    return np.array(data)

# Convert to spike times
def convert_spike_format(data, total_time):
    num_layers = 6
    I = np.argsort(data.flatten())  # Get indices of sorted latencies
    lat = np.sort(data.flatten())  # Get sorted latencies
    I = np.delete(I, np.where(lat == 0))  # Remove zero latencies indexes
    II = np.unravel_index(I, data.shape)  # Get the row, column and depth of the latencies in order
    t_step = np.ceil(np.arange(I.size) / ((I.size) / (total_time-num_layers))).astype(np.uint8)
    t_step[0]=1
    II += (t_step,)
    spike_times = np.zeros((data.shape[0], data.shape[1], total_time))
    spike_times[II] = 1
    return spike_times

#------------------------------- Weight figures function -----------------------#

def save_weights_fig(weights, path):
    for l in range(0,len(weights),2):
        full_fig_path = path + '/layer'+str(l)
        if not os.path.isdir(full_fig_path):
            os.mkdir(full_fig_path)

        layer_weights = weights[l]
        
        for f in range(len(layer_weights[0][0][0])):
            filter_weights = [[xcoord[0][f] for xcoord in ycoord] for ycoord in layer_weights]
            # print(filter_weights)
            plt.imshow(filter_weights)
            plt.colorbar()
            plt.savefig(full_fig_path +'/filter_'+str(f))
            plt.close()

# ------------------------------- Main function-------------------------------#

def main():

    # Flags
    save_weight_figs = True
    save_output_figs = True
    learn_SDNN = True  # This flag toggles between Learning STDP and classify features
                        # or just classify by loading pretrained weights for the face/motor dataset
    if learn_SDNN:
        set_weights = False  # Loads the weights from a path (path_set_weigths) and prevents any SDNN learning
        save_weights = True  # Saves the weights in a path (path_save_weigths)
        save_features = True  # Saves the features and labels in the specified path (path_features)
    else:
        set_weights = True  # Loads the weights from a path (path_set_weigths) and prevents any SDNN learning
        save_weights = False  # Saves the weights in a path (path_save_weigths)
        save_features = False  # Saves the features and labels in the specified path (path_features)

    # ------------------------------- Learn, Train and Test paths-------------------------------#
    # Image sets directories
    path = dirname(dirname(realpath(__file__)))

    # Results directories
    path_set_weigths = 'results/'
    path_save_weigths = 'results/'
    path_features = 'results/'

    # Data directory
    data_dir_name = 'collect_orientation_tap'
    home_dir = os.path.join(os.environ['DATAPATH'], 'TacTip_NM', 'edgeTap_Dobot')
    data_dir = os.path.join(home_dir, data_dir_name)
    fig_save_dir = os.path.join(os.environ['PAPERPATH'],'2020_SNNs','figures', data_dir_name)
    if not os.path.exists(fig_save_dir):
        os.mkdir(fig_save_dir)

    # Load metadata
    with open(data_dir + "/meta.json", "r") as read_file:
        meta = json.load(read_file)

    # num_classes = meta['num_num_classes']
    # num_trials = meta['num_trials']
    num_classes = 18
    num_trials = 20
    total_time = 7

    dataset = []
    labels = []

    print('Loading data... ' + str(num_classes)+' num_classes - '+str(num_trials)+' trials')

    for class_idx in range(num_classes):
        for trial_idx in range(num_trials):

            # Load data
            filename = os.path.join(data_dir, 'data_pose_' + str(class_idx) + '_trial_' + str(trial_idx)+'.pickle')
            infile = open(filename,'rb')
            data = pickle.load(infile)       
            infile.close()
            # data = remove_duplicates(data)
            data = first_spikes(data)
            data = convert_spike_format(data, total_time)
            data = np.expand_dims(data, axis=2)
            dataset.append(data)
            labels.append(class_idx)

    dataset, test_dataset, labels, test_labels = train_test_split(dataset,labels,test_size=0.2,train_size=0.8)
    train_dataset, learn_dataset, train_labels, learn_labels = train_test_split(dataset,labels,test_size = 0.25,train_size =0.75)

    spike_times_learn = np.array(learn_dataset)
    spike_times_train = np.array(train_dataset)
    spike_times_test = np.array(test_dataset)

    # ------------------------------- SDNN -------------------------------#
    # SDNN_cuda parameters
    # DoG_params = {'img_size': (128, 128), 'DoG_size': 7, 'std1': 1., 'std2': 2.}  # img_size is (col size, row size)
    DoG_params = None
    img_size = (128,128)
    network_params = [{'Type': 'input', 'num_filters': 1, 'pad': (0, 0), 'H_layer': img_size[1],
                       'W_layer': img_size[0]},
                      {'Type': 'conv', 'num_filters': 4, 'filter_size': 5, 'th': 10.},
                      {'Type': 'pool', 'num_filters': 4, 'filter_size': 7, 'th': 0., 'stride': 6},
                      {'Type': 'conv', 'num_filters': 20, 'filter_size': 17, 'th': 50.},
                      {'Type': 'pool', 'num_filters': 20, 'filter_size': 5, 'th': 0., 'stride': 5},
                      {'Type': 'conv', 'num_filters': 20, 'filter_size': 5, 'th': 2.}]

    weight_params = {'mean': 0.8, 'std': 0.01}

    max_learn_iter = [0, 300, 0, 500, 0, 600, 0]
    stdp_params = {'max_learn_iter': max_learn_iter,
                   'stdp_per_layer': [0, 10, 0, 4, 0, 2],
                   'max_iter': sum(max_learn_iter),
                   'a_minus': np.array([0, .003, 0, .0003, 0, .0003], dtype=np.float32),
                   'a_plus': np.array([0, .004, 0, .0004, 0, .0004], dtype=np.float32),
                   'offset_STDP': [0, floor(network_params[1]['filter_size']),
                                   0,
                                   floor(network_params[3]['filter_size']/8),
                                   0,
                                   floor(network_params[5]['filter_size'])]}

    # Create network
    first_net = SDNN(network_params, weight_params, stdp_params, total_time,
                     DoG_params=DoG_params, spike_times_learn=spike_times_learn,
                     spike_times_train=spike_times_train, spike_times_test=spike_times_test, y_train = train_labels, y_test = test_labels, device='CPU', save_output_figs = save_output_figs)

    # Save the initial weights figures
    if save_weight_figs:
        save_weights_fig(first_net.weights, first_net.figure_path+'/weights/initial/')

    # Set the weights or learn STDP
    if set_weights:
        weight_path_list = [path_set_weigths + 'weight_' + str(i) + '.npy' for i in range(len(network_params) - 1)]
        first_net.set_weights(weight_path_list)
    else:
        first_net.train_SDNN()

    # Save the weights
    if save_weights:
        weights = first_net.get_weights()
        for i in range(len(weights)):
            np.save(path_save_weigths + 'weight_'+str(i), weights[i])

    # Save the weights figures
    if save_weight_figs:
        save_weights_fig(first_net.weights, first_net.figure_path+'/weights/final/')

    # Get features
    X_train, y_train = first_net.train_features()
    X_test, y_test = first_net.test_features()

    # Save X_train and X_test
    if save_features:
        np.save(path_features + 'X_train', X_train)
        np.save(path_features + 'y_train', y_train)
        np.save(path_features + 'X_test', X_test)
        np.save(path_features + 'y_test', y_test)

    # ------------------------------- Classify -------------------------------#
    classifier_params = {'C': 1.0, 'gamma': 'auto'}
    train_mean = np.mean(X_train, axis=0)
    train_std = np.std(X_train, axis=0)
    X_train -= train_mean
    X_test -= train_mean
    X_train /= (train_std + 1e-5)
    X_test /= (train_std + 1e-5)
    svm = Classifier(X_train, y_train, X_test, y_test, classifier_params, classifier_type='SVM')
    train_score, test_score = svm.run_classiffier()
    print('Train Score: ' + str(train_score))
    print('Test Score: ' + str(test_score))

    print('DONE')


if __name__ == '__main__':
    start = time.time()
    main()
    end = time.time()
    print(end-start)
