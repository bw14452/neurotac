from brian2 import *
import numpy as np
import pickle
import time, os, json

import matplotlib.pyplot as plt
import matplotlib.animation as anim
import matplotlib.colors as color
import matplotlib.cm as cmx
from matplotlib.pyplot import cm

def main():
  # Set flags for data and figure types
  stimulus_type = 'object_recognition2' # edges/bumps/taps

  # # Font sizes
  # TITLE_SIZE = 20
  # AXES_SIZE = 16
  # TICKS_SIZE = 12

  # Data directory
  data_dir_name = stimulus_type
  home_dir = os.path.join(os.environ['DATAPATH'], 'TacTip_SoftHand', 'Human_Grasps')
  data_dir = os.path.join(home_dir, data_dir_name)
  fig_save_dir = os.path.join(os.environ['PAPERPATH'],'2020_IROS_NeuroTac','figures',  stimulus_type)
  if not os.path.exists(fig_save_dir):
      os.mkdir(fig_save_dir)

  # Load metadata
  with open(data_dir + "/meta.json", "r") as read_file:
      meta = json.load(read_file)

  # Set up simulation
  runtime = 500*ms
  WINDOW_WIDTH = 128
  WINDOW_HEIGHT = 128
  n_inputs = WINDOW_WIDTH*WINDOW_HEIGHT
  tau = 10*ms; El = -70*mV
  eqs = '''dvm/dt = ((El - vm) + I)/tau : volt
         I : volt'''
  input_group = NeuronGroup(n_inputs, eqs,
                    threshold='vm > -50*mV',
                    reset='vm = -70*mV',
                    method='exact')
  
  statemon = StateMonitor(input_group,'vm',record=0)
  spikemon = SpikeMonitor(input_group)
  net = Network(input_group,statemon, spikemon)
  net.store('initialized')

###########################################################################################################################################################################
  # Run simulation loop 
  # for obj_idx in range(meta['n_objs']):
  for obj_idx in range(1):
      net.restore('initialized')
      # for run_idx in range(meta['n_runs']):
      for run_idx in range(1):
          print('#### Object ' + str(obj_idx+1) + ' / ' + str(meta['n_objs']) + ' Run ' + str(run_idx+1) + ' / ' + str(meta['n_runs']) + ' ####')
          # Load data
          filename = os.path.join(data_dir, 'data_obj_' + str(obj_idx) + '_run_' + str(run_idx)+'.pickle')
          infile = open(filename,'rb')
          data, dataOff = pickle.load(infile)       
          infile.close()
          # Generate spikes for network
          spikes_i = []
          spikes_t = []
          for i in range(WINDOW_WIDTH):
            for j in range(WINDOW_HEIGHT):
              if data[i][j] != []:
                for d in data[i][j]:
                  spikes_i.append(i*WINDOW_WIDTH+j)
                  spikes_t.append(d)
          spikes_group = SpikeGeneratorGroup(n_inputs, spikes_i, spikes_t*ms)
          S = Synapses(spikes_group, input_group, on_pre='v += weight')
          S.connect()
          net.run(runtime)

  figure(figsize=(12,4))
  subplot(121)
  plot(spikemon.t/ms, spikemon.i, '.k')
  xlabel('Time (ms)')
  ylabel('Neuron index')
  subplot(122)
  plot(input_group.vm, spikemon.count/runtime)
  xlabel('vm')
  ylabel('Firing rate (sp/s)')
  plt.show()


if __name__ == '__main__':
    main()


