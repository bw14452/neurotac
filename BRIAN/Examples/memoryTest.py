from brian2 import *
import numpy as np
import pickle
import time, os, json
from collections import OrderedDict
from IPython.lib import kernel

import matplotlib.pyplot as plt

def main():
    
    start_scope()

    # Data directory
    data_dir_name = 'collect_orientation_tap'
    home_dir = os.path.join(os.environ['DATAPATH'], 'TacTip_NM', 'edgeTap_Dobot')
    data_dir = os.path.join(home_dir, data_dir_name)
    fig_save_dir = os.path.join(os.environ['PAPERPATH'],'2020_ICRA_TacTip-N_EdgeOrientation','figures', data_dir_name)
    if not os.path.exists(fig_save_dir):
      os.mkdir(fig_save_dir)

  # Load metadata
    with open(data_dir + "/meta.json", "r") as read_file:
      meta = json.load(read_file)

    # Simulation parameters
    time = 1000
    runtime = time*ms
    WINDOW_WIDTH = 128
    WINDOW_HEIGHT = 128
    n_inputs = WINDOW_WIDTH*WINDOW_HEIGHT
    
    ## Neuron equation parameters
    taum = 10*ms
    Ee = 0*mV
    vt = -50*mV
    vr = -70*mV
    El = -70*mV
    taue = 5*ms
    taui = 25*ms
    gmax = .5
    
    ## STDP synapse parameters
    taupre = 20*ms
    taupost = taupre
    dApre = .1
    dApost = -dApre * taupre / taupost * 1.05
    dApost *= gmax
    dApre *= gmax
    
    ## Winner take all parameters
    IPSC = -2*mV
    
    ## Dopamine signaling parameters
    tauc = 1000*ms
    taud = 200*ms
    tauw = 1*ms
    epsilon_dopa = 5e-3

    # Neuron equations
    eqs = '''
    dv/dt = (ge * (Ee-vr) + gi + El - v) / taum : volt
    dge/dt = -ge / taue : 1
    dgi/dt= -gi / taui : volt '''
    eqs_2 = '''
    dv/dt = (vr - v) / taum : volt '''
        
    # Layers
    input = SpikeGeneratorGroup(n_inputs, []*n_inputs, []*n_inputs*ms)   
    hidden = NeuronGroup(n_inputs, eqs, threshold='v > vt', reset='v = vr', method='exact')
    hidden.v=-70*mV
    output = NeuronGroup(meta['num_poses'], eqs, threshold='v > vt', reset='v = vr', method='exact')
    output.v=-70*mV
    
    # STDP Connections
    synapses_hidden = Synapses(input, hidden,
             '''w : 1
                dApre/dt = -Apre / taupre : 1 (event-driven)
                dApost/dt = -Apost / taupost : 1 (event-driven)''',
             on_pre='''ge += w
                    Apre += dApre
                    w = clip(w + Apost, 0, gmax)''',
             on_post='''Apost += dApost
                     w = clip(w + Apre, 0, gmax)''',
             )
    synapses_hidden.connect(j='k for k in range(i-3, i+4)', skip_if_invalid=True)
    synapses_hidden.w = 'rand() * gmax'
    
    synapses_output = Synapses(hidden, output, model =
             '''dc/dt = -c / tauc : 1 (clock-driven)
                dd/dt = -d / taud : 1 (clock-driven)
                dw/dt = c * d / tauw : 1 (clock-driven)
                dApre/dt = -Apre / taupre : 1 (event-driven)
                dApost/dt = -Apost / taupost : 1 (event-driven)''',
             on_pre='''ge += w
                    Apre += dApre
                    c = clip(c + Apost, -gmax, gmax)
                    w = clip(w+1, -gmax, gmax)''',
             on_post='''Apost += dApost
                    c = clip(c + Apre, -gmax, gmax)
                    w = clip(w+1, -gmax, gmax)''',
             )
    synapses_output.connect(p=0.2, skip_if_invalid=True)
    synapses_output.w = 'rand() * gmax'
#     synapses_output.w = '0.1 * gmax'
    
    # Teaching signal
    teaching = SpikeGeneratorGroup(1, [0]*time*meta['num_poses'],[i for i in range(time*meta['num_poses'])]*ms)
    synapses_teaching = Synapses(teaching, output, '''w : 1''', on_pre='ge+=w')
    synapses_teaching.connect()
    synapses_teaching.w = 0
    
    # Winner take all section
    synapses_inh = Synapses(output, output,
         '''w : 1''',
         on_pre='''gi+=IPSC'''
         )
    synapses_inh = Synapses(output, output, '''w : 1''', on_pre='gi+=IPSC')
    synapses_inh.connect(condition='i != j')
                         
    synapses_exc = Synapses(output, output,
         '''w : 1''',
         on_pre='''ge += 1'''
         )
    synapses_exc.connect(j='i')

    # Dopamine signaling section
    dopamine = NeuronGroup(1, eqs_2, threshold='v > vt', reset='v = vr', method='exact')
    dopamine.v = -70*mV
    synapses_classification = Synapses(output, dopamine, model='''w:1''',
                                on_pre='''v += w*mV''',
                                method='exact')

    synapses_reward = Synapses(dopamine, output, model='''w: 1''',
                                on_pre='''v += w*mV''',
                                method='exact')
    synapses_classification.connect()
    synapses_reward.connect()
    synapses_reward.w = 0.0

    # Monitors
    input_mon = SpikeMonitor(input)
    hidden_mon = SpikeMonitor(hidden)
    output_mon = SpikeMonitor(output)
    dopamine_mon = SpikeMonitor(dopamine)
    teaching_mon = SpikeMonitor(teaching)
    weights_hidden_mon = StateMonitor(synapses_hidden,'w', record=range(0,1000,10))
    weights_output_mon = StateMonitor(synapses_output,'w', record=synapses_output[:,0])
    
    # Store network state
    net = Network(input, hidden, output, teaching, dopamine, synapses_hidden, synapses_output, synapses_teaching, synapses_inh, synapses_exc, synapses_classification, synapses_reward, input_mon, hidden_mon, output_mon, teaching_mon, dopamine_mon, weights_hidden_mon, weights_output_mon)
    net.store('initialized')

    ############################################# TRAINING ###################################################
    
    # training loop 
    for pose_idx in range(meta['num_poses']):
    #   for pose_idx in range(1):
        net.restore('initialized')
        
        # Set dopamine connections to correct class
        synapses_classification.w = 0
        synapses_classification.w[pose_idx] = 10
        synapses_teaching.w[pose_idx] = 10
        
        for trial_idx in range(meta['num_trials']):
#         for trial_idx in range(2):
            print('#### Pose ' + str(pose_idx+1) + ' / ' + str(meta['num_poses']) + ' trial ' + str(trial_idx+1) + ' / ' + str(meta['num_trials']) + ' ####')
            # Load data
            filename = os.path.join(data_dir, 'data_pose_' + str(pose_idx) + '_trial_' + str(trial_idx)+'.pickle')
            infile = open(filename,'rb')
            data = pickle.load(infile)       
            infile.close()
            data = remove_duplicates(data)

            # Generate spikes for network
            spikes_i = []
            spikes_t = []
            for i in range(WINDOW_WIDTH):
                for j in range(WINDOW_HEIGHT):
                    if data[i][j] != []:
                        for spike in data[i][j]:
                            spikes_i.append(i*WINDOW_WIDTH+j)
                            spikes_t.append(spike+1000*trial_idx)
            input.set_spikes(spikes_i,spikes_t*ms)
            
            # Make connectivity figures
#             if pose_idx == 0 and trial_idx ==0:
#                 fig_connectivity(synapses_output)
                
            # Run network
            net.run(runtime)

            # Figures
#             print(list(synapses_output.w))
#             fig_spikes(input_mon,hidden_mon,output_mon,pose_idx,trial_idx,n_inputs)
            fig_teaching(output_mon, teaching_mon, weights_output_mon, synapses_output, pose_idx, trial_idx, meta['num_poses'])
#         fig_dopamine(output_mon, dopamine_mon, weights_output_mon, pose_idx, trial_idx, meta['num_poses'])
#             fig_heatmap(input_mon,hidden_mon,pose_idx,trial_idx)
#         fig_STDP(S, weights_mon, gmax,pose_idx,trial_idx)
#         fig_weights_heatmap(synapses_hidden, weights_mon, gmax,pose_idx,trial_idx)

    net.store('after_training')

    ############################################# TESTING ###################################################
    
    # testing loop 
    #    for test_number in range(5):
    #         restore('after_training')
    #         S.plastic = False  # switch plasticity off
    #         G.test_input = test_inputs[test_number]
    #         # monitor the activity now
    #         spike_mon = SpikeMonitor(G)
    #         run(...)
    
def remove_duplicates(data):
    for i in range(len(data)):
        for j in range(len(data[i])): 
            data[i][j] = list(OrderedDict.fromkeys(data[i][j]))
    return data

def fig_connectivity(S):
    fig_dir = os.path.join('figures', 'edges_learning')
    if not os.path.exists(fig_dir):
        os.mkdir(fig_dir)
#     Ns = len(S.source)
#     Nt = len(S.target)
    Ns = 1000
    Nt = len(S.target)
    figure(figsize=(5, 4))
    plot(zeros(Ns), arange(Ns), 'ok', ms=2)
    plot(ones(Nt), arange(Nt)*100, 'ok', ms=2)
#     for i, j in zip(S.i[:62], S.j[:62]):
    plot([0, 1], [S.i, S.j], '-k')
    xticks([0, 1], ['Hidden','Output'])
    ylabel('Neuron index')
    xlim(-0.1, 1.1)
    ylim(-1, max(Ns, Nt)-1)
    title('Network connectivity')
    plt.savefig(fig_dir + '/connectivity')
    plt.close()

def fig_spikes(spikes_input, spikes_hidden, spikes_output, pose_idx, trial_idx, n_inputs):
    fig_dir = os.path.join('figures', 'edges_learning', 'spikes')
    if not os.path.exists(fig_dir):
        os.mkdir(fig_dir)
    figure(figsize=(12,4))
    subplot(131)
    plot(spikes_input.t/ms, spikes_input.i, '.k')
    xlabel('Time (ms)')
    ylabel('Neuron index')
    xlim([trial_idx*1000, (trial_idx+1)*1000])
    ylim([0, n_inputs])
    title('Input spikes')
    subplot(132)
    plot(spikes_hidden.t/ms, spikes_hidden.i, '.k')
    xlabel('Time(ms)')
    ylabel('Neuron index')
    xlim([trial_idx*1000, (trial_idx+1)*1000])
    ylim([0, n_inputs])
    title('Hidden layer spikes')
    subplot(133)
    plot(spikes_output.t/ms, spikes_output.i, '.k')
    xlabel('Time(ms)')
    ylabel('Neuron index')
    xlim([trial_idx*1000, (trial_idx+1)*1000])
    ylim([-0.1, 18.1])
    yticks(np.arange(0, 18, step=1))
    title('Output spikes')
    #   plt.show()
    plt.savefig(fig_dir + '/spikes_pose_'+str(pose_idx)+'_trial_'+str(trial_idx))
    plt.close()
  
def fig_heatmap(spikes_input, spikes_hidden, pose_idx, trial_idx):
    fig_dir = os.path.join('figures', 'edges_learning', 'heatmap')
    if not os.path.exists(fig_dir):
        os.mkdir(fig_dir)
    figure(figsize=(12,4))
    subplot(121)
    x = []
    y = []
    for spike in spikes_input.i:  
      x.append(spike//128)
      y.append(spike%128)
    plt.hist2d(x, y, 64, range=np.array([(0, 128), (0, 128)]))
    xlabel('x pixel')
    ylabel('y pixel')
    xlim([0, 128])
    ylim([0, 128])
    title('Input spikes heatmap')
    subplot(122)
    x = []
    y = []
    for spike in spikes_hidden.i:  
      x.append(spike//128)
      y.append(spike%128)
    plt.hist2d(x, y, 64, range=np.array([(0, 128), (0, 128)]))
    xlim([0, 128])
    ylim([0, 128])
    xlabel('x pixel')
    ylabel('y pixel')
    title('Layer 1 spikes heatmap')
    #   plt.show()
    plt.savefig(fig_dir + '/heatmap_pose_'+str(pose_idx)+'_trial_'+str(trial_idx))
    plt.close()

# def fig_STDP(synapses, weights_mon, gmax, pose_idx, trial_idx):
#     fig_dir = os.path.join('figures', 'edges_learning', 'STDP')
#     if not os.path.exists(fig_dir):
#         os.mkdir(fig_dir)
# #     subplot(311)
# #     plot(synapses.w / gmax, '.k')
# #     ylabel('Weight / gmax')
# #     xlabel('Synapse index')
#     subplot(312)
#     hist(synapses.w / gmax, 20)
#     xlabel('Weight / gmax')
#     subplot(313)
#     plot(weights_mon.t/second, weights_mon.w.T/gmax)
#     xlabel('Time (s)')
#     ylabel('Weight / gmax')
#     tight_layout()
#     plt.savefig(fig_dir + '/STDP_pose_'+str(pose_idx)+'_trial_'+str(trial_idx))
# #     show()

def fig_weights_heatmap(synapses, weights_mon, gmax, pose_idx, trial_idx):
    fig_dir = os.path.join('figures', 'edges_learning', 'weights_heatmap')
    if not os.path.exists(fig_dir):
        os.mkdir(fig_dir)
    figure(figsize=(5, 4))
    
    dx, dy = 1, 1
    y, x = np.mgrid[slice(0, 127 + dy, dy),
                slice(0, 127 + dx, dx)]
    z = [[0]*128 for x in range(128)]
              
    for i in range(len(synapses.w)):
        synapse = synapses.j[i]
        weight = synapses.w[i]
        row = synapse//128
        column = synapse%128
        z[row][column] =z[row][column] + weight

    plt.pcolormesh(x,y,z)
    xlabel('Time (s)')
    ylabel('Weight / gmax')
    tight_layout()
    plt.savefig(fig_dir + '/STDP_pose_'+str(pose_idx)+'_trial_'+str(trial_idx))
    plt.close()

def fig_dopamine(spikes_output, spikes_dopamine, weights_output_mon, pose_idx, trial_idx, num_poses):
    fig_dir = os.path.join('figures', 'edges_learning', 'dopamine')
    if not os.path.exists(fig_dir):
        os.mkdir(fig_dir)
    figure(figsize=(12,4))
    subplot(131)
    axhline(y=pose_idx)
    plot(spikes_output.t/ms, spikes_output.i, '.k')
    xlabel('Time(ms)')
    ylabel('Neuron index')
    xlim([0, trial_idx*1000])
    ylim([-0.1, 18.1])
    yticks(np.arange(0, 18, step=1))
    title('Output spikes')
    subplot(132)   
    plot(spikes_dopamine.t/ms, spikes_dopamine.i, '.k')
    xlabel('Time (ms)')
    ylabel('Neuron index')
    xlim([0, trial_idx*1000])
    title('Dopamine spikes')
    subplot(133) 
    for j in range(num_poses):
        plot(weights_output_mon.t/ms, weights_output_mon.w[j], '.')
    xlabel('Time (ms)')
    ylabel('Neuron index')
    xlim([0, trial_idx*1000])
    plt.legend([str(a) for a in range(num_poses)],loc='upper right', bbox_to_anchor=(1.35, 1.15))
    title('Output weights')
    tight_layout()
    plt.savefig(fig_dir + '/dopamine_pose_'+str(pose_idx))
    plt.close()

def fig_teaching(spikes_output, spikes_teaching, weights_output_mon, synapses_output, pose_idx, trial_idx, num_poses):
    fig_dir = os.path.join('figures', 'edges_learning', 'teaching')
    if not os.path.exists(fig_dir):
        os.mkdir(fig_dir)
    figure(figsize=(12,4))
    subplot(131)
    axhline(y=pose_idx)
    plot(spikes_output.t/ms, spikes_output.i, '.k')
    xlabel('Time(ms)')
    ylabel('Neuron index')
    xlim([0, trial_idx*1000])
    ylim([-0.1, 18.1])
    yticks(np.arange(0, 18, step=1))
    title('Output spikes')
    subplot(132)   
    plot(spikes_teaching.t/ms, spikes_teaching.i, '.k')
    xlabel('Time (ms)')
    ylabel('Neuron index')
    xlim([0, trial_idx*1000])
    title('Teaching spikes')
    subplot(133)
#     print(len(weights_output_mon))
#     outputs = [[] for i in range(18)]
#     for i in range(len(weights_output_mon)):
#         outputs[weights_output_mon.i[i]] = weights_output_mon.w[i]
#     print(weights_output_mon[synapses_output[:,0]].w)
    plot(weights_output_mon.t/ms, weights_output_mon[synapses_output[:,0]].w.T, '.')
    xlabel('Time (ms)')
    ylabel('Neuron index')
    xlim([0, trial_idx*1000])
    plt.legend([str(a) for a in range(num_poses)],loc='upper right', bbox_to_anchor=(1.35, 1.15))
    title('Output weights')
    tight_layout()
    plt.savefig(fig_dir + '/teaching_pose_'+str(pose_idx))
    plt.close()
    
if __name__ == '__main__':
    main()
    