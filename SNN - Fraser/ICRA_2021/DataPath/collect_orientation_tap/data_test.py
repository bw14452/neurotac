import pickle 
import numpy as np

infile = open('data_pose_1_trial_10.pickle','rb')
data = pickle.load(infile)

max_t = 0
min_t = 10000
for d in data:
  for spikes in d: 
    if len(spikes)!=0:
      temp_max_t = np.max(spikes)
      if temp_max_t > max_t:
        max_t = temp_max_t
      temp_min_t = np.min(spikes)
      if temp_min_t < min_t:
        min_t = temp_min_t
print(max_t)
print(min_t)

