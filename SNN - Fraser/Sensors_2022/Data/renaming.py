for depth in meta_dict['slide_depths']:
    for speed in meta_dict['slide_speeds']:
        for direction in meta_dict['slide_directions']:
            for r in range(meta_dict['n_runs']):
                os.rename('tactip_in_dir_'+ str(direction) + '_d' + str(depth) + '_s' + str(speed) + '_r' + str(r)+'.mp4', 'tactip_in_d'+ str(depth) + '_s' + str(speed) + '_dir_' + direction + '_r' + str(r)+'.mp4')
                os.rename('tactip_out_dir_'+ str(direction) + '_d' + str(depth) + '_s' + str(speed) + '_r' + str(r)+'.mp4', 'tactip_out_d'+ str(depth) + '_s' + str(speed) + '_dir_' + direction + '_r' + str(r)+'.mp4')