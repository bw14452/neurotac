import pickle 
import numpy as np 
from pyNN.parameters import Sequence
import math


# #Load in a pickle file 
# def load_pickle (filename, obj_idx):
#     load_path = "Old_pynn/" + str(filename) +"/" + (str(obj_idx) + ".pickle")  
#     pickle_in = open(load_path, "rb") 
#     pickle_out = pickle.load(pickle_in)
#     return pickle_out

def Remove(duplicate): 
    final_list = [] 
    for num in duplicate: 
        if num not in final_list: 
            final_list.append(num) 
    return final_list  

def Sequencer (pre_data, no_of_pixels):
    inputarray=[None] * (no_of_pixels)
    for x in range (0, no_of_pixels):
        if len(pre_data[x]) > 0:
            inputarray[x] = Sequence(pre_data[x])
        else:
            inputarray[x] = Sequence([])
    return inputarray


#Save a pickle file externally
def save_pickle (obj_idx, file_name):
    save_path = ((str(file_name) + ".pickle"))
    pickle_out = open((save_path), 'wb')
    pickle.dump(obj_idx, pickle_out)    
    pickle_out.close()
 
#Load in sample data
def load_pickle (obj_idx, path):
    load_path = (str(path) + (str(obj_idx) + ".pickle"))
    pickle_in = open((load_path), "rb")
    pickle_out = pickle.load(pickle_in)
    return pickle_out

#############################

def Heatmapping (spiketrain, dimensionX, dimensionY):
    FlatSpikes = np.reshape(spiketrain, (dimensionX, dimensionY))
    for i in range (FlatSpikes.shape[0]):
        for j in range (FlatSpikes.shape[1]):
            FlatSpikes[i,j] = len(FlatSpikes[i,j])
    FlatSpikes = np.vstack(FlatSpikes[:, :]).astype(np.float)
    return (FlatSpikes)

def TrimSpikes (IncomingSpikes, Quartile, dimension):
    #SpikeTimes = loaded_pickle(IncomingSpikes)
    SpikeTimes = IncomingSpikes
    SpikeDensity = Heatmapping(SpikeTimes, dimension, dimension)
    SpikeTruth = SpikeDensity
    spikeMed = np.median(np.nonzero(SpikeDensity))
    spikeQuar = np.quantile(np.nonzero(SpikeDensity), Quartile)
    #run through and remove all spikes that are below the 1st quartile
    for i in range (SpikeDensity.shape[0]):
        for j in range (SpikeDensity.shape[1]):
            if SpikeDensity[i, j] < spikeQuar:
                SpikeTruth[i, j] = 0
            else:
                SpikeTruth[i, j] = 1
    SpikeTruth = SpikeTruth.flatten('C')
    for k in range (len(SpikeTimes)):
        if SpikeTruth[k] == 0:
            SpikeTimes[k] = 0
    return (SpikeDensity)

def GetActivationLocation (SpikeDensity, dimension):
    MinX = dimension
    MinY = 0
    MaxX = 0
    MaxY = 0

    for y in range (SpikeDensity.shape[0]):
        z = np.nonzero(SpikeDensity[y])[0]
        if z.size is not 0:
            if z[0] < MinX:
                MinX = z[0]
    for y in range (SpikeDensity.shape[1]):
        z = np.nonzero(SpikeDensity[y])[0]
        if z.size is not 0:
            MinY = y
            break
    for y in range (SpikeDensity.shape[1]):
        z = (np.nonzero(SpikeDensity[y]))[0]
        if z.size is not 0:
            if z[-1] > MaxX:
                MaxX = z[-1]
    for y in range (SpikeDensity.shape[1]):
        z = (np.nonzero(SpikeDensity[y]))[0]
        if z.size is not 0:
            if z[-1] > MaxY:
                MaxY = y
    XLength = MaxX - MinX
    YLength = MaxY - MinY 
    SizeOfArea = XLength * YLength
    return (MinX, MinY, MaxX, MaxY, XLength, YLength, SizeOfArea)

#######################################################

def PollingNeuronActivationNeuronIndex (ActivationPoints, PoolingLayerSize) :
    ListOfPoolNeurons = np.zeros([ActivationPoints[4], ActivationPoints[5]])
    PostPoolStartPoint = (ActivationPoints[0] * ActivationPoints[1])
    for a in range (len(ListOfPoolNeurons[0])):
        for b in range (len(ListOfPoolNeurons[1])):
            ListOfPoolNeurons[a, b] = (math.sqrt(PoolingLayerSize) * (PostPoolStartPoint) + b + (math.sqrt(PoolingLayerSize) * a))
    ListOfPoolNeurons = ListOfPoolNeurons.flatten('C')
    return (ListOfPoolNeurons)

#######################################################

def VerticalLine(XLength, YLength):
    sequentialMatrix = np.zeros((XLength, YLength))
    linepoint = XLength // 2
    for a in range (YLength):
        for b in range (XLength):
            if b == linepoint: 
                sequentialMatrix[a,b] = 1 
    sequentialMatrix.flatten('C')
    return(sequentialMatrix)

def HorizontalLine(sampleradius):
    sequentialMatrix = np.zeros((sampleradius,sampleradius))
    for a in range (5):
        if a == 2:
            sequentialMatrix[a] = [1, 1, 1, 1, 1]
    return (sequentialMatrix)

def BRightTLeft(sampleradius):
    sequentialMatrix = np.zeros((sampleradius,sampleradius))
    for a in range (5):
        sequentialMatrix[a, a] = 1
    return sequentialMatrix

def TRightBLeft(sampleradius):
    sequentialMatrix = np.zeros((sampleradius,sampleradius))
    np.fill_diagonal(np.fliplr(sequentialMatrix), [1, 1, 1, 1, 1])
    return sequentialMatrix



