import pickle, os, timeit
import matplotlib.pyplot as plt
import numpy as np
import neo

import pyNN.nest as sim
from pyNN.utility import get_simulator 
from pyNN.random import RandomDistribution
from pyNN.parameters import Sequence
from functionsForPynn import Sequencer
from sklearn.model_selection import train_test_split

############################################ Functions #########################################################################
def load_data(pose_idx, trial_idx):           # -- Load and flatten data to 1D array

    filename = os.path.join('Data', 'tap_data_dobot','data_pose_'+str(pose_idx)+'_trial_'+str(trial_idx)+'.pickle')
    with open(filename, 'rb') as datafile: 
        data = pickle.load(datafile)
    pre_data = [np.sort(list(set(spikes))) for spikes in data.flatten('C') ]
    simtime = max(max([list(spikes) for spikes in pre_data]))
    n_pixels = len(pre_data)
    input_array = Sequencer(pre_data, n_pixels)      # -- Convert 1D array to a pyNN Sequence class
   
    return input_array, simtime

def load_test_training_data(n_poses, n_trials):
    x = []
    y = np.repeat(range(n_poses),n_trials)
    for pose_idx in range(n_poses):
        for trial_idx in range(n_trials):
            filename = os.path.join('Data', 'tap_data_dobot','data_pose_'+str(pose_idx)+'_trial_'+str(trial_idx)+'.pickle')
            with open(filename, 'rb') as datafile: 
                data = pickle.load(datafile)
            pre_data = [np.sort(list(set(spikes))) for spikes in data.flatten('C') ]
            x.append(pre_data)

    x_train, x_test, y_train, y_test = train_test_split(x,y, test_size=0.2)
    return x_train, x_test, y_train, y_test

def sequence_data(data, n_pixels):# Add delay between trials
    combined_data = [None]*n_pixels
    for pixel in range(n_pixels):
        combined_data[pixel] = np.sort(np.concatenate([trial[pixel] for trial in data]))
    inputarray = [Sequence(spikes) for spikes in combined_data]
    return inputarray

cell_parameters = {
    "tau_m": 10.0,       # (ms)
    "v_thresh": -50.0,   # (mV)
    "v_reset": -60.0,    # (mV)
    "v_rest": -60.0,     # (mV)
    "cm": 1.0,           # (nF)
    "tau_refrac": 5.0,  # (ms) long refractory period to prevent bursting
}
# cell_parameters = {
#     "tau_m": 10.0,       # cell membrane time constant -- (ms)
#     "tau_refrac": 10,    # refractory period duration -- (ms)
#     "cm": 1.0,           # membrane capacity -- (nF)
#     # "tau_syn_E": 5,      # rise time of the excitatory synaptic function -- (ms)
#     # "tau_syn_I": 5,      # rise time of the inhibitory synaptic function -- (ms)
#     "i_offset": 0.0,     # Offset current.
#     "v_thresh": -50.0,   # spike threshold -- (mV)
#     "v_reset": -60.0,    # reset potential post-spike -- (mV)
#     "v_rest": -70.0,     # resting membrane potential -- (mV)
# }

# Initialize simulation
sim, options = get_simulator(
                    ("--save-population", "Save spike_times to be reloaded into a SpikeSourceArray later", {"action": "store_true"}),
                    ("--plot-figure", "Plot the simulation results to a file", {"action": "store_true"}),
                    ("--fit-curve", "Calculate the best-fit curve to the weight-delta_t measurements", {"action": "store_true"}),
                    ("--dendritic-delay-fraction", "What fraction of the total transmission delay is due to dendritic propagation", {"default": 1}),
                    ("--debug", "Print debugging information")
                    )
if options.debug:
    init_logging(None, debug=True)
# sim.setup(timestep = 0.1)

plot_spikes = True
plot_weights = True

stdp_model = sim.STDPMechanism(
    timing_dependence=sim.SpikePairRule(tau_plus=20.0, tau_minus=20.0,
                                        A_plus=0.01, A_minus=0.012),
    weight_dependence=sim.AdditiveWeightDependence(w_min=0, w_max=2),
    weight=5,
    delay=1)

input_spikes, _ = load_data(0, 0)  
pooling_factor = 64
neuron_type = sim.IF_curr_exp(**cell_parameters)
n_pixels = len(input_spikes)

x_train, x_test, y_train, y_test = load_test_training_data(2, 2)

n_pixels = len(x_train[0])
simtime = max(max([list(spikes) for tap in x_train for spikes in tap]))
input_spikes_train = sequence_data(x_train, n_pixels)      # -- Convert 1D array to a pyNN Sequence class


#################################### Load data  ############################################
tic = timeit.default_timer()

############################## Initialize network ############################################
sim.setup(timestep = 0.1)

# Layers
input_layer = sim.Population(n_pixels, sim.SpikeSourceArray (spike_times=input_spikes_train), label = 'InputLayer')            
hidden_layer = sim.Population(n_pixels//pooling_factor, neuron_type, label='Hidden Layer')       
output_layer = sim.Population(100, neuron_type, label='Output Layer')    

if options.plot_figure: 
    if plot_spikes:
        input_layer.record('spikes', sampling_interval=1.0)
        hidden_layer.record('spikes', sampling_interval=1.0)
        output_layer.record('spikes', sampling_interval=1.0)

# Connections
input_hidden_connection = sim.Projection(input_layer, hidden_layer, sim.FixedNumberPreConnector(256), sim.StaticSynapse(weight=1.0))
hidden_output_connection = sim.Projection(hidden_layer, output_layer, sim.FixedNumberPreConnector(64), stdp_model) 
# ctx_cells.sample(10).record(('v', 'w')) #, sampling_interval=0.2)

# Set input spikes and weights  
if trial_idx == 0 and pose_idx ==0:
    # hidden_output_weights = [0.4]*hidden_layer.size*output_layer.size
    hidden_output_weights = RandomDistribution('uniform', (0.0,0.7))
#     input_hidden_weights = RandomDistribution('normal', mu=0.8, sigma=0.2)        
# input_hidden_connection.set(weight=input_hidden_weights)
hidden_output_connection.set(weight=hidden_output_weights)
toc = timeit.default_timer()
print("Data load and weight set time = " + str(toc-tic))

############################################ Run simulation #####################################################
tic = timeit.default_timer()
print('Running simulation pose '+ str(pose_idx) + ' trial ' + str(trial_idx) )
sim.run(simtime)
toc = timeit.default_timer()
print("Simulation time = " + str(toc-tic))

############################################ Save/ plot data #####################################################
# spikes_filename = os.path.join('Figures', os.path.basename(__file__)[:-3], 'spikes','spikes_pose_'+str(pose_idx)+'_trial_'+str(trial_idx)+'.pkl')
# weights_filename = os.path.join('Figures', os.path.basename(__file__)[:-3], 'weights', 'weights_pose_'+str(pose_idx)+'_trial_'+str(trial_idx)+'.pkl')

# input_hidden_weights = input_hidden_connection.get('weight', format='list',  with_address=False)
hidden_output_weights = hidden_output_connection.get('weight', format='list',  with_address=False)
# neo.AnalogSignal(hidden_output_weights, units='nA', sampling_period=self.interval * ms,
#                           name="weight", array_annotations={"channel_index": np.arange(len(self._weights[0]))})

input_data = input_layer.get_data().segments[0]
hidden_data = hidden_layer.get_data().segments[0]
output_data = output_layer.get_data().segments[0]

tic = timeit.default_timer()
if options.plot_figure:
    from pyNN.utility.plotting import Figure, Panel, DataTable
    spikes_figure_filename = spikes_filename.replace("pkl", "png")
    if plot_spikes:
        Figure(
            # raster plot of the presynaptic neuron spike times
            Panel(input_data.spiketrains,
                yticks=True, markersize=0.2, xlim=(0, simtime), xticks = True, xlabel = 'time(ms)'),
            Panel(hidden_data.spiketrains,
                yticks=True, markersize=0.2, xlim=(0, simtime), xticks = True, xlabel = 'time(ms)'),
            Panel(output_data.spiketrains,
                yticks=True, markersize=0.2, xlim=(0, simtime), xticks = True, xlabel = 'time(ms)'),
            title="Spikes",
        ).save(spikes_figure_filename)
    if plot_weights:
        weights_figure_filename = weights_filename.replace("pkl", "png")
        weights_input_figure_filename = os.path.join('Figures', os.path.basename(__file__)[:-3], 'weights', 'weights_input_hidden_pose_'+str(pose_idx)+'_trial_'+str(trial_idx)+'.png')
        plt.clf()
        plt.plot(hidden_output_weights)
        plt.savefig(weights_figure_filename)
        # Figure(
        #     Panel(hidden_output_weights,
        #         xticks=True, yticks=True, xlim=(0, n_pixels),
        #         xlabel="neuron", ylabel="weights",
        #         show_fit=options.fit_curve),
        #     title="Input",
        # ).save(weights_figure_filename)

toc = timeit.default_timer()
print("Figure creation time = " + str(toc-tic))
sim.end()
