import pickle
from pyNN.utility import get_simulator, normalized_filename, init_logging
from pyNN.parameters import Sequence
from pyNN.utility.plotting import DataTable, Figure, Panel
from functionsForPynn import load_pickle, PollingNeuronActivationNeuronIndex, save_pickle, loaded_pickle, Sequencer, Remove, HorizontalLine, VerticalLine, TRightBLeft, BRightTLeft, TrimSpikes, GetActivationLocation, Heatmapping
import pyNN.space
import neo
from quantities import ms
import numpy as np
import math
import matplotlib.pyplot as plt
from sklearn import svm
import csv
import random
import numpy as np
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
NumberOfRuns = 20
NumberOfObjects = 17
NeuronModules = 1
CollectInputActivity = 0
CollectFilterActivity = 0
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
for t in range (NumberOfObjects):
    for d in range (NumberOfRuns):
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
        sim, options = get_simulator(("--plot-figure", "Plot the simulation results to a file", {"action": "store_true"}),
                             ("--fit-curve", "Calculate the best-fit curve to the weight-delta_t measurements", {"action": "store_true"}),
                             ("--dendritic-delay-fraction", "What fraction of the total transmission delay is due to dendritic propagation", {"default": 1}),
                             ("--debug", "Print debugging information"))
        if options.debug:
            init_logging(None, debug=True)
        sim.setup(timestep=0.01, min_delay=delay, max_delay=delay)

        STDPWeighting = True # -- Are we using STDP for weighting on this

        if NeuronModules == 1:
            cell_parameters = {
                "tau_m": 10.0,       # cell membrane time constant -- (ms)
                "tau_refrac": 10,    # refractory period duration -- (ms)
                "cm": 1.0,           # membrane capacity -- (nF)
                "tau_syn_E": 5,      # rise time of the excitatory synaptic function -- (ms)
                "tau_syn_I": 5,      # rise time of the inhibitory synaptic function -- (ms)
                "i_offset": 0.0,     # Offset current.
                "v_thresh": -50.0,   # spike threshold -- (mV)
                "v_reset": -60.0,    # reset potential post-spike -- (mV)
                "v_rest": -70.0,     # resting membrane potential -- (mV)
            }
            neuron_type = sim.IF_curr_alpha(**cell_parameters)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
               
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                                # Input Layer & Spiking Data #
    #
        loaded_data = load_pickle("SingleTap_run_" + str(d) + "_orientation_" + str(t))
        pre_data = [None] * loaded_data.size
        pre_data = loaded_data.flatten('C')                 # -- Load and flatten data to 1D array
        no_of_pixels = pre_data.size
        simtime = max(max(pre_data))
        for x in range(0,no_of_pixels):
            pre_data[x] = Remove(pre_data[x])               # -- Remove duplicate timestamps
        for i in range(len(pre_data)):                      # -- Reorder arrays
            pre_data[i] = np.sort(pre_data[i])
        inputarray = Sequencer(pre_data, no_of_pixels)      # -- Convert 1D array to a pyNN Sequence class
    #
        FirstLayerStructure = sim.space.Grid2D(aspect_ratio  = 1, dx = 1.0, dy = 1.0, fill_order = 'sequential')  
        FirstLayerStructure.generate_positions(no_of_pixels)

        InputLayer = sim.Population(no_of_pixels, sim.SpikeSourceArray (spike_times = inputarray), structure = FirstLayerStructure, label = 'InputLayer')      
        FirstLayer = sim.Population(no_of_pixels, neuron_type, structure = FirstLayerStructure, label='First_Layer')                                            

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                                    # Pooling layer #

        diloution_factor_second_layer = 64                              # -- Declare the size of second layers; 64 = 16/16 grid
        PoolingIncrement = math.sqrt(diloution_factor_second_layer)     # -- Amount of change
        PoolingStartPoint =  PoolingIncrement / 2                       # -- Origin point
        PoolLayerNeurons = no_of_pixels // diloution_factor_second_layer# -- Number of neurons
        DDPC = sim.DistanceDependentProbabilityConnector                # -- Create a feedthrough
        InputConnector = DDPC("d<"+str(PoolingIncrement+2))             # -- Create a feedthrough
        FirstTrueConnector = DDPC("d<"+str(PoolingIncrement))           # -- Define the square area of feedthrough
    #
        PoolingStructure = sim.space.Grid2D(aspect_ratio = 1, dx = PoolingIncrement, dy = PoolingIncrement, x0 = PoolingStartPoint, y0 = PoolingStartPoint, fill_order='sequential')
        PoolingStructure.generate_positions(PoolLayerNeurons)
        PoolingLayer = sim.Population(PoolLayerNeurons, neuron_type, structure = PoolingStructure, label='Pooling Layer')       
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                                # Weights #
    if STDPWeighting:
        stdp_model = sim.STDPMechanism(
                timing_dependence=sim.SpikePairRule(tau_plus=20.0, tau_minus=20.0, A_plus=0.01, A_minus=0.012),
                weight_dependence=sim.AdditiveWeightDependence(w_min=0, w_max=10),
                weight=10,
                delay=1,
                dendritic_delay_fraction=float(options.dendritic_delay_fraction))
    else:

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                                    # Projection #

        InputConnection = sim.Projection(InputLayer, FirstLayer, FirstTrueConnector, sim.StaticSynapse (weight = 0.5))
        PoolingConnection = sim.Projection(FirstLayer, PoolingLayer, sim.AllToAllConnector(), sim.stdp_model)  
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                                # Recording spikes #

        if CollectInputActivity == 1:
            InputLayer.record('spikes')
        if CollectFilterActivity == 1:
            FirstLayer.record('spikes')
        PoolingLayer.record('spikes')

                                # Run simulation #
        print()
        print()
        print()
        print("Currently running simulation: Run #" + str(d) +" of orientation " + str(t*10) +" degrees" )
        print("... processing Input & Pooling layer")
        print()
        print()
        print()
        sim.run(simtime)
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                                    # Conclude data #

        PooledSpikes_pre = PoolingLayer.get_data().segments[0]
        if CollectInputActivity == 1:
            InputSpikes_pre = InputLayer.get_data().segments[0]
        if CollectFilterActivity == 1:
            FirstSpikes_pre = FirstLayer.get_data().segments[0]

        sim.end()    
        sim.reset()
    # # 
        if CollectInputActivity == 1:
           InputTimes = [None] * no_of_pixels
           for i in range (0, no_of_pixels):
               InputTimes[i] = InputSpikes_pre.spiketrains[i]
           GoSpikes = Heatmapping(InputTimes, 128, 128)
           InputNumb = GoSpikes.flatten('C')
           InputNumb = InputNumb.astype(int)
           save_pickle('SNN_Output/', InputNumb, 'Input_Model_' +str(NeuronModules) +'_run_' + str(d) + '_degrees_' + str(t) )
           save_pickle('SNN_Output/', InputTimes, 'InputTime_Model_' +str(NeuronModules) +'_run_' + str(d) + '_degrees_' + str(t) )
    # #
        if CollectFilterActivity == 1:
            FirstTimes = [None] * no_of_pixels
            for i in range (0, no_of_pixels):
                FirstTimes[i] = FirstSpikes_pre.spiketrains[i]
            WhatSpikes = Heatmapping(FirstTimes, 128, 128)
            FirstNumb = WhatSpikes.flatten('C')
            FirstNumb = FirstNumb.astype(int)
            save_pickle('SNN_Output/', FirstNumb, 'Filter_Model_' +str(NeuronModules) +'_run_' + str(d) + '_degrees_' + str(t) )
            save_pickle('SNN_Output/', FirstTimes, 'FilterTime_Model_' +str(NeuronModules) +'_run_' + str(d) + '_degrees_' + str(t) )

    # #
        FinalTimes = [None] * PoolLayerNeurons
        for i in range (0, PoolLayerNeurons):
            FinalTimes[i] = PooledSpikes_pre.spiketrains[i]
        PooledSpikers = Heatmapping(FinalTimes, 16, 16)
        FinalNumb = PooledSpikers.flatten('C')
        FinalNumb = FinalNumb.astype(int)
        save_pickle('SNN_Output/', FinalNumb, 'Output_Model_' +str(NeuronModules) +'_run_' + str(d) + '_degrees_' + str(t) )
        save_pickle('SNN_Output/', FinalTimes, 'OutputTime_Model_' +str(NeuronModules) +'_run_' + str(d) + '_degrees_' + str(t) )
        
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
                            # SVM after pooling #
   

    sim.reset()
   
print ('Completed simulation;')
print ('  Simulated ' +str(d + 1) + ' runs of ' + str(t + 1) + ' orientations')