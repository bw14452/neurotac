import os, pickle
import numpy as np
from collections import OrderedDict
from sklearn.manifold import TSNE
from sklearn.datasets import load_digits

from matplotlib import pyplot as plt
import seaborn as sns

DATATYPE = 'spatial' # spatial or temporal

def main():
  # Load data
  data_dir_name = 'collect_orientation_tap'    
  home_dir = os.path.join('Sensors_2022','Data')
  data_dir = os.path.join(home_dir, data_dir_name)
  fig_save_dir = os.path.join('Sensors_2022','Figures')
  if not os.path.exists(fig_save_dir):
      os.makedirs(fig_save_dir)

  n_trials = 20
  n_poses = 18
  sns.set(rc={'figure.figsize':(11.7,8.27)})
  palette = sns.color_palette("bright", n_poses)

  data_x = []
  y = np.array([])

  # Load data
  for pose_idx in range(n_poses):
    for trial_idx in range(n_trials):
      filename = os.path.join(data_dir, 'data_pose_' + str(pose_idx) + '_trial_' + str(trial_idx)+'.pickle')
      infile = open(filename,'rb')
      data = pickle.load(infile)       
      infile.close()
      data = remove_duplicates(data)
      # X.append([len(px) for row in data for px in row])
      data_x.append(data)
      y = np.append(y, pose_idx)

  X = []
  if DATATYPE == 'spatial':
    X = distribution_in_space(data_x)
  elif DATATYPE == 'temporal':
    X = distribution_in_time(data_x)

  # Run tsne
  tsne = TSNE()
  X_embedded = tsne.fit_transform(X)

  # Create figure
  create_fig(X_embedded, y, DATATYPE)


############################################# FUNCTIONS ###################################################
def remove_duplicates(data):
    for i in range(len(data)):
        for j in range(len(data[i])): 
            data[i][j] = list(OrderedDict.fromkeys(data[i][j]))
    return data

def distribution_in_space(data):
  X = []
  for i in range(len(data)):
    X.append([len(px) for row in data[i] for px in row])
  return X

def distribution_in_time(data):
  X = []
  for i in range(len(data)):
    X.append([len(px) for row in data[i] for px in row])
  return X

def create_fig (X_embedded, y, DATATYPE):  
  sns.scatterplot(X_embedded[:,0], X_embedded[:,1], hue=y, legend='full', palette=palette)
  if DATATYPE == 'spatial':  
    plt.savefig("tsne_fig_rawData_spatial.png")
  if DATATYPE == 'temporal':  
    plt.savefig("tsne_fig_rawData_temporal.png")


if __name__ == '__main__':
    # os.environ['DATAPATH'] = "/home/fraser/Desktop/neurotac/SNN - Fraser/ICRA_2021/DataPath" #remove this
    # os.environ['PAPERPATH'] = "/home/fraser/Desktop/neurotac/SNN-Fraser/ICRA_2021/PaperPath" #Paper location
    main()

