
import pickle, os, timeit, random, math
import matplotlib.pyplot as plt
import numpy as np
import neo
import seaborn as sns

import pyNN.nest as sim
from pyNN.utility import get_simulator 
from pyNN.random import RandomDistribution
from pyNN.parameters import Sequence
from sklearn.model_selection import train_test_split

# data_folder = os.path.join('Data', 'tap_data_dobot')

############################################ Functions #########################################################################
# def load_data(pose_idx, trial_idx):           # -- Load and flatten data to 1D array
#     filename = os.path.join('Data', 'tap_data_dobot','data_pose_'+str(pose_idx)+'_trial_'+str(trial_idx)+'.pickle')
#     with open(filename, 'rb') as datafile:
#         data = pickle.load(datafile)
#     pre_data = [np.sort(list(set(spikes))) for spikes in data.flatten('C') ]
#     simtime = max(max([list(spikes) for spikes in pre_data]))
#     n_pixels = len(pre_data)
#     input_array = Sequencer(pre_data, n_pixels)      # -- Convert 1D array to a pyNN Sequence class
   
#     return input_array, simtime

def load_data(data_folder, n_poses, n_trials):
    x = []
    y = np.repeat(range(n_poses),n_trials)
    simtimes = []
    for pose_idx in range(n_poses):
        for trial_idx in range(n_trials):
            filename = os.path.join(data_folder,'data_pose_'+str(pose_idx)+'_trial_'+str(trial_idx)+'.pickle')
            with open(filename, 'rb') as datafile:
                data = pickle.load(datafile)
            pre_data = [np.sort(list(set(spikes))) for spikes in data.flatten('C') ]
            simtime = max(max([list(spikes) for spikes in pre_data]))
            sequenced_data = [Sequence(spikes) for spikes in pre_data]
            x.append(sequenced_data)
            simtimes.append(simtime)

    x_train, x_test, y_train, y_test = train_test_split(x,y, test_size=0.2)
    with open('train_test_data.pkl', 'wb') as f:  # Python 3: open(..., 'wb')
        pickle.dump([x_train, x_test, y_train, y_test, simtimes], f)
    return x_train, x_test, y_train, y_test, simtimes

def save_output (obj_idx, file_name):
    save_path = ((str(file_name) + ".pickle"))
    pickle_out = open((save_path), 'wb')
    pickle.dump(obj_idx, pickle_out)    
    pickle_out.close()

def NeuronFrequency (spiketrain, dimensionX, dimensionY):
    numbspikes = [None] * (dimensionX * dimensionY)
    for i in range (0, (dimensionX * dimensionY)):
        numbspikes[i] = spiketrain[i]
    FlatSpikes = np.reshape(spiketrain, (dimensionX, dimensionY))
    for i in range (FlatSpikes.shape[0]):
        for j in range (FlatSpikes.shape[1]):
            FlatSpikes[i,j] = len(FlatSpikes[i,j])
    FlatSpikes = np.vstack(FlatSpikes[:, :]).astype(np.float)
    return (FlatSpikes)

def run_network_classifier(data_folder):  

    cell_parameters = {
        "tau_m": 10.0,       # (ms)
        "v_thresh": -50.0,   # (mV)
        "v_reset": -60.0,    # (mV)
        "v_rest": -60.0,     # (mV)
        "cm": 1.0,           # (nF)
        "tau_refrac": 5.0,  # (ms) long refractory period to prevent bursting
    }

    # Initialize simulation
    sim, options = get_simulator(
                        ("--save-population", "Save spike_times to be reloaded into a SpikeSourceArray later", {"action": "store_true"}),
                        ("--plot-figure", "Plot the simulation results to a file", {"action": "store_true"}),
                        ("--fit-curve", "Calculate the best-fit curve to the weight-delta_t measurements", {"action": "store_true"}),
                        ("--dendritic-delay-fraction", "What fraction of the total transmission delay is due to dendritic propagation", {"default": 1}),
                        ("--debug", "Print debugging information")
                        )
    if options.debug:
        init_logging(None, debug=True)

    plot_spikes = True
    plot_heatmaps = True
    plot_weights = False

    ############################################################################ Network setup ###################################################################

    x_train, x_test, y_train, y_test, simtimes = load_data(data_folder,18, 20)  

    # with open('train_test_data.pkl', 'rb') as f:  
    #     x_train, x_test, y_train, y_test, simtimes = pickle.load(f)

    # Training or testing

    Type_of_run = 'training'
    #Type_of_run = 'testing'
    if Type_of_run == 'training':
        x_data = x_train
        y_data = y_train
    else:
        x_data = x_test
        y_data = y_test

    pooling_2d = 64
    pooling_1d = math.sqrt(pooling_2d)
    neuron_type = sim.IF_curr_exp(**cell_parameters)
    n_pixels = len(x_data[0])

    sim.setup(timestep = 0.1, t_flush = 100)

    # STDP setup
    hidden_output_weights = RandomDistribution('uniform', (0.0,0.9))
    # hidden_output_weights = 0.2
    stdp_model = sim.STDPMechanism(
        timing_dependence=sim.SpikePairRule(tau_plus=20.0, tau_minus=20.0,
                                            A_plus=0.01, A_minus=0.012),
        weight_dependence=sim.AdditiveWeightDependence(w_min=0, w_max=2),
        weight= 0.2,
        delay=0)

    # Layers
    input_layer_structure = sim.space.Grid2D(aspect_ratio  = 1, dx = 1.0, dy = 1.0, fill_order = 'sequential')  
    input_layer_structure.generate_positions(n_pixels)
    input_layer = sim.Population(n_pixels, sim.SpikeSourceArray(), structure = input_layer_structure, label = 'InputLayer')  

    hidden_layer_structure = sim.space.Grid2D(aspect_ratio = 1, dx = pooling_1d, dy = pooling_1d, x0 = pooling_1d/2, y0 = pooling_1d/2)
    hidden_layer_structure.generate_positions(n_pixels//pooling_2d)
    hidden_layer = sim.Population(n_pixels//pooling_2d, neuron_type, structure = hidden_layer_structure, label='Hidden Layer')      

    output_layer = sim.Population(100, neuron_type, label='Output Layer')    

    if options.plot_figure:
        if plot_spikes:
            input_layer.record('spikes', sampling_interval=1.0)
            hidden_layer.record('spikes', sampling_interval=1.0)
            output_layer.record('spikes', sampling_interval=1.0)

    # Projections      
    input_hidden_connector = sim.DistanceDependentProbabilityConnector("d<"+str(pooling_1d))    
    # input_hidden_connector = sim.DistanceDependentProbabilityConnector("d<20")    

    input_hidden_projection = sim.Projection(input_layer, hidden_layer, input_hidden_connector, sim.StaticSynapse(weight=1.0))
    hidden_output_projection = sim.Projection(hidden_layer, output_layer, sim.FixedNumberPreConnector(16), stdp_model)
    # hidden_output_projection = sim.Projection(hidden_layer, output_layer, sim.AllToAllConnector(), stdp_model)

    input_hidden_projection.save('weight', 'input_hidden_projection.txt', format='list')
    hidden_output_projection.save('weight', 'hidden_output_projection_start.txt', format='list')


    ############################################################################ Main Loop ###################################################################
    for iteration in range(len(x_data)):

        sim.reset()
        ############################## Update network #########################################
        # Update input spikes
        print('\nRunning simulation iteration '+ str(iteration) + '/'+ str(len(x_data))+ ' (pose ' + str(y_data[iteration]) +')' )
        tic = timeit.default_timer()
        input_spikes = x_data[iteration]
        simtime = simtimes[iteration]
        # input_spikes, simtime = load_data(pose_idx, trial_idx)    
        input_layer.set(spike_times=input_spikes)


    ############################################### Check input spikes ###################################################################
        input_spikes_plottable = []
        for i in range(len(input_spikes)):
            if input_spikes[i].value.size == 0:
                input_spikes_plottable.append([])
            elif input_spikes[i].value.size >= 1:
                input_spikes_plottable.append(list(input_spikes[i].value))

        input_layer_spikes_plottable = []
        input_layer_spikes = input_layer.get('spike_times')
        for i in range(len(input_layer_spikes)):
            if input_layer_spikes[i].value.size == 0:
                input_layer_spikes_plottable.append([])
            elif input_layer_spikes[i].value.size >= 1:
                input_layer_spikes_plottable.append(list(input_layer_spikes[i].value[0]))
        # input_layer_spikes_plottable = [x.value[0][0] for x in input_layer.get('spike_times')]
        # plt.clf()
        # plt.eventplot([x for x in input_spikes_plottable if x != []], linewidths=2)
        # plt.savefig('input_spikes_test_ iteration'+ str(iteration) +'.png')
        # plt.clf()
        # plt.eventplot([x for x in input_layer_spikes_plottable if x != []], linewidths=2)
        # plt.savefig('input_layer_spikes_test_ iteration'+ str(iteration) +'.png')

        # Update weights to output layer
        hidden_output_projection.set(weight=hidden_output_weights)
        # hidden_output_projection = sim.Projection(hidden_layer, output_layer, sim.AllToAllConnector(), stdp_model)
        toc = timeit.default_timer()
        print("Data load and weight set time = " + str(toc-tic))

        ################################# Run simulation ###################################
        tic = timeit.default_timer()
        sim.run(simtime)
        toc = timeit.default_timer()
        print("Simulation time = " + str(toc-tic))
        # print(str(sim.get_current_time()))
        ############################################ Save/ plot data #####################################################
        spikes_filename = os.path.join('Figures', os.path.basename(__file__)[:-3], 'spikes','spikes_iteration '+ str(iteration) + ' (pose ' + str(y_data[iteration]) +')' +'.pkl')
        weights_filename = os.path.join('Figures', os.path.basename(__file__)[:-3], 'weights', 'weights_iteration '+ str(iteration) + ' (pose ' + str(y_data[iteration]) +')' +'.pkl')
        input_heatmaps_filename = os.path.join('Figures', os.path.basename(__file__)[:-3], 'heatmaps', 'input_heatmaps_iteration '+ str(iteration) + ' (pose ' + str(y_data[iteration]) +')' +'.pkl')
        hidden_heatmaps_filename = os.path.join('Figures', os.path.basename(__file__)[:-3], 'heatmaps', 'hidden_heatmaps_iteration '+ str(iteration) + ' (pose ' + str(y_data[iteration]) +')' +'.pkl')
        output_heatmaps_filename = os.path.join('Figures', os.path.basename(__file__)[:-3], 'heatmaps', 'output_heatmaps_iteration '+ str(iteration) + ' (pose ' + str(y_data[iteration]) +')' +'.pkl')


        hidden_output_weights = sorted(hidden_output_projection.get('weight', format='list',  with_address=True))
        hidden_output_weights = [w[2] for w in hidden_output_weights]

        # Save the raw data and clear it out in prep. for network reset
        input_data = input_layer.get_data(clear=True).segments[0]
        hidden_data = hidden_layer.get_data(clear=True).segments[0]
        output_data = output_layer.get_data(clear=True).segments[0]

        #Collect spike count for both input and hidden layer activity
        InputSpikesFrequency = NeuronFrequency(input_data.spiketrains, 128, 128)
        HiddenSpikesFrequency = NeuronFrequency(hidden_data.spiketrains, 16, 16)
        OutputSpikesFrequency = NeuronFrequency(output_data.spiketrains, 10, 10)
        SpikesForOutput = HiddenSpikesFrequency.flatten('C'),y_data[iteration]

        with open ('output_spikes_' + str(Type_of_run) +'.pickle', 'wb') as f:
            pickle.dump (SpikesForOutput, f)

        if options.plot_figure:
            tic = timeit.default_timer()
            from pyNN.utility.plotting import Figure, Panel, DataTable
            spikes_figure_filename = spikes_filename.replace("pkl", "png")
            if plot_spikes:
                Figure(
                    # raster plot of the presynaptic neuron spike times
                    Panel(input_data.spiketrains,
                        yticks=True, markersize=0.2, xlim=(0, simtime), xticks = True, xlabel = 'time(ms)'),
                    Panel(hidden_data.spiketrains,
                        yticks=True, markersize=0.2, xlim=(0, simtime), xticks = True, xlabel = 'time(ms)'),
                    Panel(output_data.spiketrains,
                        yticks=True, markersize=0.2, xlim=(0, simtime), xticks = True, xlabel = 'time(ms)'),
                    title="Spikes",
                ).save(spikes_figure_filename)

            if plot_heatmaps:
                input_heatmap_filename = input_heatmaps_filename.replace("pkl", "png")
                plt.close()
                sns.heatmap(InputSpikesFrequency, cmap="YlGnBu").invert_yaxis()
                plt.savefig(input_heatmap_filename)
                plt.close()
                
                hidden_heatmap_filename = hidden_heatmaps_filename.replace("pkl", "png")
                plt.close()
                sns.heatmap(HiddenSpikesFrequency, cmap = "YlGnBu").invert_yaxis()
                plt.savefig(hidden_heatmap_filename)
                plt.close()

            # as final layer is 100 neurons, heatmap is visually irrelevant  
                # output_heatmap_filename = output_heatmaps_filename.replace("pkl", "png")
                # plt.close()
                # plt.bar(OutputSpikesFrequency, align='center').invert_yaxis()
                # plt.savefig(output_heatmap_filename)
                # plt.close()

            if plot_weights:
                weights_figure_filename = weights_filename.replace("pkl", "png")
                weights_input_figure_filename = os.path.join('Figures', os.path.basename(__file__)[:-3], 'weights', 'weights_input_hidden_iteration '+ str(iteration) + '/'+ str(len(x_data))+ ' (pose ' + str(y_data[iteration]) +')' +'.png')
                plt.clf()
                plt.plot(hidden_output_weights)
                plt.savefig(weights_figure_filename)
                plt.close()
            toc = timeit.default_timer()
            print("Figure creation time = " + str(toc-tic))
            print("Average weight hidden-output: " + str(np.mean(hidden_output_weights)))
            print("Maximum weight hidden-output: " + str(np.max(hidden_output_weights))
        
        
            )


    sim.end()

    hidden_output_projection.save('weight', 'hidden_output_projection_end.txt', format='list')

def main():
    data_folder = os.path.join('Data', 'tap_data_dobot')
    run_network_classifier(data_folder)

if __name__ == '__main__':
    main()