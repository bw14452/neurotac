import matplotlib.pyplot as plt
import pickle
import numpy as np

with open('losses_optimizer_stdp.pkl', 'rb') as f:  
    losses = pickle.load(f)


# plt.clf()
# plt.plot(errors)
# ax = plt.axes(projection='3d')
# ax.contour3D(  n_output_neurons, pooling_factors, errors, 50, cmap='binary')
# plt.show()

plt.clf()
plt.plot(losses)
plt.xlabel('Classification Error')
plt.ylabel('Iteration')
plt.title('STDP parameters optimization')
# plt.colorbar()
plt.savefig('Figures/losses_stdp_optimizer.png')
plt.show()