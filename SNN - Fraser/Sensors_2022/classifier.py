import pickle
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis


def classify(filename):

    with open(filename, 'rb') as f:  
        x_train, y_train, x_test, y_test, simtimes = pickle.load(f)

    # x_train, x_test, y_train, y_test = train_test_split(x,y, test_size=0.2)
    scores = []
    for i in range(20):
        clf = KNeighborsClassifier(i+1)
        clf.fit(x_train, y_train)
        y_pred = clf.predict(x_test)  
        score = clf.score(x_test, y_test)
        scores.append(score)
    
    return scores


def main():
    filename = 'data_taps_post_network.pkl'
    scores = classify(filename)
    print(scores)
    print(max(scores))
    print(np.argmax(scores))

if __name__ == "__main__":
    main()