
import pickle, os, timeit, random, math
import matplotlib.pyplot as plt
import numpy as np
import neo
import seaborn as sns
from hyperopt import hp, fmin, tpe
from hpsklearn import HyperoptEstimator
import optuna
import matplotlib.pyplot as plt

import pyNN.nest as sim
from pyNN.utility import get_simulator 
from pyNN.random import RandomDistribution
from pyNN.parameters import Sequence
from sklearn.model_selection import train_test_split
from sklearn.metrics import plot_confusion_matrix
from sklearn.metrics import ConfusionMatrixDisplay

from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.cluster import KMeans

def run_training_and_testing(datatype, A):  

    pooling_factor = 16
    n_output_neurons = 100
    tau = 20.0

    data_folder = os.path.join('Data', datatype + '_data_dobot')
    cell_parameters = {
        "tau_m": 10.0,       # (ms)
        "v_thresh": -50.0,   # (mV)
        "v_reset": -60.0,    # (mV)
        "v_rest": -60.0,     # (mV)
        "cm": 1.0,           # (nF)
        "tau_refrac": 5.0,  # (ms) long refractory period to prevent bursting
    }

    plot_figures = False
    plot_spikes = False
    plot_heatmaps = False
    plot_weights = False

    ############################################################################ Network setup ###################################################################

    # x, y, simtimes = load_data(data_folder, n_poses=9, n_trials=10)  

    with open('data_taps_pre_network_optimizer.pkl', 'rb') as f:  
        x, y, simtimes = pickle.load(f)

    x_train, x_test, y_train, y_test = train_test_split(x,y, test_size=0.2)

    pooling_1d = pooling_factor
    pooling_2d = pooling_1d**2
    neuron_type = sim.IF_curr_exp(**cell_parameters)
    n_pixels = len(x_train[0])

    sim.setup(timestep = 0.5, t_flush = 100)

    # STDP setup
    # hidden_output_weights = RandomDistribution('uniform', (0.0,0.9))
    hidden_output_weights = 0.3
    stdp_model = sim.STDPMechanism(
        timing_dependence=sim.SpikePairRule(tau_plus=tau, tau_minus=tau,
                                            A_plus=A, A_minus=A*1.2),
        weight_dependence=sim.AdditiveWeightDependence(w_min=0, w_max=5),
        weight= 0.2,
        delay=0)

    # Layers
    input_layer_structure = sim.space.Grid2D(aspect_ratio  = 1, dx = 1.0, dy = 1.0, fill_order = 'sequential')  
    input_layer_structure.generate_positions(n_pixels)
    input_layer = sim.Population(n_pixels, sim.SpikeSourceArray(), structure = input_layer_structure, label = 'InputLayer')  

    hidden_layer_structure = sim.space.Grid2D(aspect_ratio = 1, dx = pooling_1d, dy = pooling_1d, x0 = pooling_1d//2, y0 = pooling_1d//2)
    hidden_layer_structure.generate_positions(n_pixels//pooling_2d)
    hidden_layer = sim.Population(n_pixels//pooling_2d, neuron_type, structure = hidden_layer_structure, label='Hidden Layer')      

    output_layer = sim.Population(n_output_neurons, neuron_type, label='Output Layer')   
    output_layer.record('spikes', sampling_interval=1.0)

    # Projections      
    input_hidden_connector = sim.DistanceDependentProbabilityConnector("d<"+str(pooling_factor))    

    input_hidden_projection = sim.Projection(input_layer, hidden_layer, input_hidden_connector, sim.StaticSynapse(weight=1.0))
    hidden_output_projection = sim.Projection(hidden_layer, output_layer, sim.FixedNumberPreConnector(16), sim.StaticSynapse(weight=0.3))
    # hidden_output_projection = sim.Projection(hidden_layer, output_layer, sim.FixedNumberPreConnector(16), stdp_model)
    # hidden_output_projection = sim.Projection(hidden_layer, output_layer, sim.AllToAllConnector(), stdp_model)

    input_hidden_projection.save('weight', 'input_hidden_projection.txt', format='list')
    hidden_output_projection.save('weight', 'hidden_output_projection_start.txt', format='list')


    ############################################################################ Main Loop ###################################################################
    x_train_output = []
    x_test_output = []
    for run in ['train', 'test']:
        if run == 'train':
            x_data = x_train
            y_data = y_train
        else:
            x_data = x_test
            y_data = y_test

        for iteration in range(len(x_data)):
            sim.reset()
            ############################## Update network #########################################
            # Update input spikes
            print('\nRunning ' + run + 'ing simulation iteration '+ str(iteration) + '/'+ str(len(x_data))+ ' (pose ' + str(y_data[iteration]) +')' )
            tic = timeit.default_timer()
            input_spikes = x_data[iteration]
            simtime = simtimes[iteration]
            # input_spikes, simtime = load_data(pose_idx, trial_idx)    
            input_layer.set(spike_times=input_spikes)

            # Update weights to output layer
            # hidden_output_projection.set(weight=hidden_output_weights)
            # hidden_output_projection = sim.Projection(hidden_layer, output_layer, sim.AllToAllConnector(), stdp_model)
            toc = timeit.default_timer()
            print("Input spikes and weight set time = " + str(toc-tic))

            ################################# Run simulation ###################################
            tic = timeit.default_timer()
            sim.run(simtime)
            toc = timeit.default_timer()
            print("Simulation time = " + str(toc-tic))
            # print(str(sim.get_current_time()))

            # hidden_output_weights = sorted(hidden_output_projection.get('weight', format='list',  with_address=True))
            # hidden_output_weights = [w[2] for w in hidden_output_weights]

            output_data = output_layer.get_data(clear=True).segments[0]
            if run == 'train':
                x_train_output.append([len(spikes) for spikes in output_data.spiketrains])
            else:
                x_test_output.append([len(spikes) for spikes in output_data.spiketrains])

            # print("Average weight hidden-output: " + str(np.mean(hidden_output_weights)))

    sim.end()

    ######################################################## Classify network output ################################################################
    tic = timeit.default_timer()
    score = classify(datatype, x_train_output, y_train, x_test_output, y_test, pooling_factor, n_output_neurons)
    toc = timeit.default_timer()
    print("Classification time = " + str(toc-tic))

    with open('data_taps_post_network.pkl', 'wb') as f:  
        pickle.dump([x_train_output, y_train, x_test_output, y_test, simtimes ], f)

    return score 


######################################################## Helper Functions #########################################################################
def load_data(data_folder, n_poses = 18, n_trials = 20):
    x = []
    y = np.repeat(range(n_poses),n_trials)
    simtimes = []
    for pose_idx in range(n_poses):
        for trial_idx in range(n_trials):
            filename = os.path.join(data_folder,'data_pose_'+str(pose_idx)+'_trial_'+str(trial_idx)+'.pickle')
            with open(filename, 'rb') as datafile:
                data = pickle.load(datafile)
            pre_data = [np.sort(list(set(spikes))) for spikes in data.flatten('C') ]
            simtime = max(max([list(spikes) for spikes in pre_data]))
            sequenced_data = [Sequence(spikes) for spikes in pre_data]
            x.append(sequenced_data)
            simtimes.append(simtime)

    with open('data_taps_pre_network.pkl', 'wb') as f:  # Python 3: open(..., 'wb')
        pickle.dump([x, y, simtimes], f)
    return x, y, simtimes

def save_output (obj_idx, file_name):
    save_path = ((str(file_name) + ".pickle"))
    pickle_out = open((save_path), 'wb')
    pickle.dump(obj_idx, pickle_out)    
    pickle_out.close()

def classify(datatype, x_train, y_train, x_test, y_test, pooling_factor, n_output_neurons):

    clf = KNeighborsClassifier(n_neighbors=1)
    # clf = KMeans(n_clusters=clusters, random_state=0)
    clf.fit(x_train, y_train)
    score = clf.score(x_test, y_test)
    plot_confusion_matrix(clf, x_test, y_test)  
    ConfusionMatrixDisplay.from_estimator(clf, x_test, y_test)
    confusion_matrix_filename = os.path.join('Figures', datatype, 'training_and_testing', 'confusion_matrices', 'confusion_matrix_pooling_'+str(pooling_factor)+'_output_'+str(n_output_neurons)+'.png')
    plt.savefig(confusion_matrix_filename)
    # plt.show()

    # print('KMeans score: ' + str(score))
    print('KNN score: ' + str(score))

    return abs(1-score)


def main():
    datatype = 'tap'
    scores = []
    for A in [0.01]: 
        score = run_training_and_testing(datatype, A)
        scores.append([A, score])
    
    with open('scores_pooling_factors.pkl', 'wb') as f:  
        pickle.dump(scores, f)

    print(scores)


if __name__ == "__main__":
    main()