import matplotlib.pyplot as plt
import pickle
import numpy as np

with open('scores_pooling_factors_taps.pkl', 'rb') as f:  
    scores = pickle.load(f)

pooling_factors = [4,8,16,32]
n_output_neurons = range(50,500,50)
errors = [score[2] for score in scores]
errors= np.reshape(errors, (len(pooling_factors), len(n_output_neurons)))

# plt.clf()
# plt.plot(errors)
# ax = plt.axes(projection='3d')
# ax.contour3D(  n_output_neurons, pooling_factors, errors, 50, cmap='binary')
# plt.show()

plt.clf()
plt.contourf(n_output_neurons, pooling_factors, errors, 20, cmap='RdGy')
plt.ylabel('Pooling factor')
plt.xlabel('Output neurons')
plt.title('Network structure effect on classification error')
plt.colorbar()
plt.savefig('Figures/pooling_factors_output_neurons.png')
plt.show()