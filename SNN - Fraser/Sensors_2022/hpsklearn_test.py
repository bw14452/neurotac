import pickle

from hpsklearn import HyperoptEstimator, sklearn_KNeighborsClassifier
from hyperopt import tpe

# Load Data
with open('data_taps_post_network_full_pooling_4_rfield_4_output_100.pkl', 'rb') as f:  
    x_train, y_train, x_test, y_test, simtimes = pickle.load(f)

# Create the estimator object
estim = HyperoptEstimator(classifier=sklearn_KNeighborsClassifier(""))

# Search the space of classifiers and preprocessing steps and their
# respective hyperparameters in sklearn to fit a model to the data
estim.fit( x_train, y_train )

# Report the accuracy of the classifier on a given set of data
score = estim.score( x_test, y_test)

# Return instances of the classifier and preprocessing steps
model = estim.best_model()

print(score)
print(model)