
import pickle, os, timeit, random, math
import matplotlib.pyplot as plt
import numpy as np
import neo
import seaborn as sns
from hyperopt import hp

import pyNN.nest as sim
from pyNN.utility import get_simulator 
from pyNN.random import RandomDistribution
from pyNN.parameters import Sequence
from sklearn.model_selection import train_test_split

from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis

def run_network_classifier(data_folder):  

    cell_parameters = {
        "tau_m": 10.0,       # (ms)
        "v_thresh": -50.0,   # (mV)
        "v_reset": -60.0,    # (mV)
        "v_rest": -60.0,     # (mV)
        "cm": 1.0,           # (nF)
        "tau_refrac": 5.0,  # (ms) long refractory period to prevent bursting
    }

    plot_figures = True
    plot_spikes = True
    plot_heatmaps = False
    plot_weights = False

    ############################################################################ Network setup ###################################################################

    x_train, x_test, y_train, y_test, simtimes = load_data(data_folder, 4, 5)  

    # with open('data_taps_pre_network.pkl', 'rb') as f:  
    #     x_train, x_test, y_train, y_test, simtimes = pickle.load(f)

    pooling_2d = 64
    pooling_1d = math.sqrt(pooling_2d)
    neuron_type = sim.IF_curr_exp(**cell_parameters)
    n_pixels = len(x_train[0])

    sim.setup(timestep = 0.5, t_flush = 100)

    # STDP setup
    hidden_output_weights = RandomDistribution('uniform', (0.0,0.7))
    # hidden_output_weights = 0.2
    stdp_model = sim.STDPMechanism(
        timing_dependence=sim.SpikePairRule(tau_plus=20.0, tau_minus=20.0,
                                            A_plus=0.01, A_minus=0.012),
        weight_dependence=sim.AdditiveWeightDependence(w_min=0, w_max=2),
        weight= 0.2,
        delay=0)

    # Layers
    input_layer_structure = sim.space.Grid2D(aspect_ratio  = 1, dx = 1.0, dy = 1.0, fill_order = 'sequential')  
    input_layer_structure.generate_positions(n_pixels)
    input_layer = sim.Population(n_pixels, sim.SpikeSourceArray(), structure = input_layer_structure, label = 'InputLayer')  

    hidden_layer_structure = sim.space.Grid2D(aspect_ratio = 1, dx = pooling_1d, dy = pooling_1d, x0 = pooling_1d/2, y0 = pooling_1d/2)
    hidden_layer_structure.generate_positions(n_pixels//pooling_2d)
    hidden_layer = sim.Population(n_pixels//pooling_2d, neuron_type, structure = hidden_layer_structure, label='Hidden Layer')      

    output_layer = sim.Population(100, neuron_type, label='Output Layer')   
    # input_layer.record('spikes', sampling_interval=1.0)
    # hidden_layer.record('spikes', sampling_interval=1.0)
    output_layer.record('spikes', sampling_interval=1.0)

    # if plot_figures:
    #     if plot_spikes:
    #         input_layer.record('spikes', sampling_interval=1.0)
    #         hidden_layer.record('spikes', sampling_interval=1.0)
    #         output_layer.record('spikes', sampling_interval=1.0)

    # Projections      
    input_hidden_connector = sim.DistanceDependentProbabilityConnector("d<"+str(pooling_1d))    
    # input_hidden_connector = sim.DistanceDependentProbabilityConnector("d<20")    

    input_hidden_projection = sim.Projection(input_layer, hidden_layer, input_hidden_connector, sim.StaticSynapse(weight=1.0))
    hidden_output_projection = sim.Projection(hidden_layer, output_layer, sim.FixedNumberPreConnector(32), stdp_model)
    # hidden_output_projection = sim.Projection(hidden_layer, output_layer, sim.AllToAllConnector(), stdp_model)

    input_hidden_projection.save('weight', 'input_hidden_projection.txt', format='list')
    hidden_output_projection.save('weight', 'hidden_output_projection_start.txt', format='list')


    ############################################################################ Main Loop ###################################################################
    x_train_output = []
    x_test_output = []
    for run in ['train', 'test']:
        if run == 'train':
            x_data = x_train
            y_data = y_train
        else:
            x_data = x_test
            y_data = y_test

        for iteration in range(len(x_data)):
            sim.reset()
            ############################## Update network #########################################
            # Update input spikes
            print('\nRunning simulation iteration '+ str(iteration) + '/'+ str(len(x_data))+ ' (pose ' + str(y_data[iteration]) +')' )
            tic = timeit.default_timer()
            input_spikes = x_data[iteration]
            simtime = simtimes[iteration]
            # input_spikes, simtime = load_data(pose_idx, trial_idx)    
            input_layer.set(spike_times=input_spikes)

            # Update weights to output layer
            hidden_output_projection.set(weight=hidden_output_weights)
            # hidden_output_projection = sim.Projection(hidden_layer, output_layer, sim.AllToAllConnector(), stdp_model)
            toc = timeit.default_timer()
            print("Input spikes and weight set time = " + str(toc-tic))

            ################################# Run simulation ###################################
            tic = timeit.default_timer()
            sim.run(simtime)
            toc = timeit.default_timer()
            print("Simulation time = " + str(toc-tic))
            # print(str(sim.get_current_time()))

            ############################################ Save/ plot data #####################################################
            # spikes_filename = os.path.join('Figures', os.path.basename(__file__)[:-3], 'spikes','spikes_iteration '+ str(iteration) + ' (pose ' + str(y_data[iteration]) +')' +'.pkl')
            # weights_filename = os.path.join('Figures', os.path.basename(__file__)[:-3], 'weights', 'weights_iteration '+ str(iteration) + ' (pose ' + str(y_data[iteration]) +')' +'.pkl')
            # input_heatmaps_filename = os.path.join('Figures', os.path.basename(__file__)[:-3], 'heatmaps', 'input_heatmaps_iteration '+ str(iteration) + ' (pose ' + str(y_data[iteration]) +')' +'.pkl')
            # hidden_heatmaps_filename = os.path.join('Figures', os.path.basename(__file__)[:-3], 'heatmaps', 'hidden_heatmaps_iteration '+ str(iteration) + ' (pose ' + str(y_data[iteration]) +')' +'.pkl')
            # output_heatmaps_filename = os.path.join('Figures', os.path.basename(__file__)[:-3], 'heatmaps', 'output_heatmaps_iteration '+ str(iteration) + ' (pose ' + str(y_data[iteration]) +')' +'.pkl')

            hidden_output_weights = sorted(hidden_output_projection.get('weight', format='list',  with_address=True))
            hidden_output_weights = [w[2] for w in hidden_output_weights]

            # Save the raw data and clear it out in prep. for network reset
            # input_data = input_layer.get_data(clear=True).segments[0]
            # hidden_data = hidden_layer.get_data(clear=True).segments[0]
            output_data = output_layer.get_data(clear=True).segments[0]

            #Collect spike count for both input and hidden layer activity
            # InputSpikesFrequency = NeuronFrequency(input_data.spiketrains, 128, 128)
            # HiddenSpikesFrequency = NeuronFrequency(hidden_data.spiketrains, 16, 16)
            if run == 'train':
                x_train_output.append([len(spikes) for spikes in output_data.spiketrains])
            else:
                x_test_output.append([len(spikes) for spikes in output_data.spiketrains])

            # with open ('output_spikes_' + str() +'.pickle', 'wb') as f:
            #     pickle.dump (SpikesForOutput, f)

            # if plot_figures:
            #     tic = timeit.default_timer()
            #     from pyNN.utility.plotting import Figure, Panel, DataTable
            #     spikes_figure_filename = spikes_filename.replace("pkl", "png")
            #     if plot_spikes:
            #         Figure(
            #             # raster plot of the presynaptic neuron spike times
            #             Panel(input_data.spiketrains,
            #                 yticks=True, markersize=0.2, xlim=(0, simtime), xticks = True, xlabel = 'time(ms)'),
            #             Panel(hidden_data.spiketrains,
            #                 yticks=True, markersize=0.2, xlim=(0, simtime), xticks = True, xlabel = 'time(ms)'),
            #             Panel(output_data.spiketrains,
            #                 yticks=True, markersize=0.2, xlim=(0, simtime), xticks = True, xlabel = 'time(ms)'),
            #             title="Spikes",
            #         ).save(spikes_figure_filename)

            #     if plot_heatmaps:
            #         input_heatmap_filename = input_heatmaps_filename.replace("pkl", "png")
            #         plt.close()
            #         sns.heatmap(InputSpikesFrequency, cmap="YlGnBu").invert_yaxis()
            #         plt.savefig(input_heatmap_filename)
            #         plt.close()
                    
            #         hidden_heatmap_filename = hidden_heatmaps_filename.replace("pkl", "png")
            #         plt.close()
            #         sns.heatmap(HiddenSpikesFrequency, cmap = "YlGnBu").invert_yaxis()
            #         plt.savefig(hidden_heatmap_filename)
            #         plt.close()

            #     # as final layer is 100 neurons, heatmap is visually irrelevant  
            #         # output_heatmap_filename = output_heatmaps_filename.replace("pkl", "png")
            #         # plt.close()
            #         # plt.bar(OutputSpikesFrequency, align='center').invert_yaxis()
            #         # plt.savefig(output_heatmap_filename)
            #         # plt.close()

            #     if plot_weights:
            #         weights_figure_filename = weights_filename.replace("pkl", "png")
            #         weights_input_figure_filename = os.path.join('Figures', os.path.basename(__file__)[:-3], 'weights', 'weights_input_hidden_iteration '+ str(iteration) + '/'+ str(len(x_data))+ ' (pose ' + str(y_data[iteration]) +')' +'.png')
            #         plt.clf()
            #         plt.plot(hidden_output_weights)
            #         plt.savefig(weights_figure_filename)
            #         plt.close()
            #     toc = timeit.default_timer()
            #     print("Figure creation time = " + str(toc-tic))
            print("Average weight hidden-output: " + str(np.mean(hidden_output_weights)))
                # print("Maximum weight hidden-output: " + str(np.max(hidden_output_weights)))

    sim.end()
    tic = timeit.default_timer()
    score = classify(x_train_output, y_train, x_test_output, y_test)
    toc = timeit.default_timer()
    print("Classification time = " + str(toc-tic))
    return score 

def main():
    data_folder = os.path.join('Data', 'tap_data_dobot')
    run_network_classifier(data_folder)
    # Run optimizer 


######################################################## Helper Functions #########################################################################
def load_data(data_folder, n_poses, n_trials):
    x = []
    y = np.repeat(range(n_poses),n_trials)
    simtimes = []
    for pose_idx in range(n_poses):
        for trial_idx in range(n_trials):
            filename = os.path.join(data_folder,'data_pose_'+str(pose_idx)+'_trial_'+str(trial_idx)+'.pickle')
            with open(filename, 'rb') as datafile:
                data = pickle.load(datafile)
            pre_data = [np.sort(list(set(spikes))) for spikes in data.flatten('C') ]
            simtime = max(max([list(spikes) for spikes in pre_data]))
            sequenced_data = [Sequence(spikes) for spikes in pre_data]
            x.append(sequenced_data)
            simtimes.append(simtime)

    x_train, x_test, y_train, y_test = train_test_split(x,y, test_size=0.2)
    with open('data_taps_pre_network.pkl', 'wb') as f:  # Python 3: open(..., 'wb')
        pickle.dump([x_train, x_test, y_train, y_test, simtimes], f)
    return x_train, x_test, y_train, y_test, simtimes

def save_output (obj_idx, file_name):
    save_path = ((str(file_name) + ".pickle"))
    pickle_out = open((save_path), 'wb')
    pickle.dump(obj_idx, pickle_out)    
    pickle_out.close()

def classify(x_train, y_train, x_test, y_test):
    names = ["Nearest Neighbors", "Linear SVM", "RBF SVM", "Gaussian Process",
         "Decision Tree", "Random Forest", "Neural Net", "AdaBoost",
         "Naive Bayes", "QDA"]

    classifiers = [
        KNeighborsClassifier(3),
        SVC(kernel="linear", C=0.025),
        SVC(gamma=2, C=1),
        GaussianProcessClassifier(1.0 * RBF(1.0)),
        DecisionTreeClassifier(max_depth=5),
        RandomForestClassifier(max_depth=5, n_estimators=10, max_features=1),
        MLPClassifier(alpha=1, max_iter=1000),
        AdaBoostClassifier(),
        GaussianNB(),
        QuadraticDiscriminantAnalysis()]

    for name, clf in zip(names, classifiers):
        # ax = plt.axes()
        scores = []
        clf.fit(x_train, y_train)
        y_pred = clf.predict(x_test)  
        scores.append(clf.score(x_test, y_test))
        print(name + ': ' + str(score))

    score = max(scores)

if __name__ == "__main__":
    main()