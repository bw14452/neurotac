import os, pickle
import numpy as np
from collections import OrderedDict
from sklearn.manifold import TSNE
from sklearn.datasets import load_digits

from matplotlib import pyplot as plt
import seaborn as sns

DATASET = 'slide' # slide or tap
DATATYPE = 'temporal' # spatial or temporal

def main():
  # Load data
  data_dir_name = 'collect_orientation_' + DATASET    

  home_dir = os.path.join('Data')
  data_dir = os.path.join(home_dir, data_dir_name)
  fig_save_dir = os.path.join('Figures')
  if not os.path.exists(fig_save_dir):
      os.makedirs(fig_save_dir)

  n_trials = 20
  n_poses = 18
  sns.set(rc={'figure.figsize':(11.7,8.27)})
  palette = sns.color_palette("hls", n_poses)

  data_x = []
  y = np.array([])

  # Load data
  for pose_idx in range(n_poses):
    for trial_idx in range(n_trials):
      filename = os.path.join(data_dir, 'data_pose_' + str(pose_idx) + '_trial_' + str(trial_idx)+'.pickle')
      infile = open(filename,'rb')
      data = pickle.load(infile)       
      infile.close()
      data = remove_duplicates(data)
      # X.append([len(px) for row in data for px in row])
      data_x.append(data)
      y = np.append(y, pose_idx*10+10)

  X = []
  if DATATYPE == 'spatial':
    X = distribution_in_space(data_x)
  elif DATATYPE == 'temporal':
    X = distribution_in_time(data_x)

  # Run tsne
  tsne = TSNE(perplexity=20,n_iter=1000, learning_rate=400)
  X_embedded = tsne.fit_transform(X)

  # Create figure
  create_fig(fig_save_dir, X_embedded, y, DATATYPE, DATASET, palette)


############################################# FUNCTIONS ###################################################
def remove_duplicates(data):
    for i in range(len(data)):
        for j in range(len(data[i])): 
            data[i][j] = list(OrderedDict.fromkeys(data[i][j]))
    return data

def distribution_in_space(data):
  X = []
  for i in range(len(data)):
    X.append([len(px) for row in data[i] for px in row])
  return X

def distribution_in_time(data):
  X = [[spike for row in tap for spiketrain in row for spike in spiketrain if len(spiketrain)!=0] for tap in data]
  bins = np.linspace(0,1000,1001)
  X = [np.histogram(x,bins)[0] for x in X]
  return X

def distribution_in_spacetime(data):
  X = [[spike for row in tap for spiketrain in row for spike in spiketrain if len(spiketrain)!=0] for tap in data]
  bins = np.linspace(0,1000,1001)
  X = [np.histogram(x,bins)[0] for x in X]
  return X

def create_fig (fig_save_dir, X_embedded, y, DATATYPE, DATASET, palette):  
  sns.scatterplot(X_embedded[:,0], X_embedded[:,1], hue=y, palette=palette)
  angles = [i*10+10 for i in range(18)]
  y_labels = [str(i)+ ' $^\circ$' for i in angles]
  plt.legend(y_labels)
  plt.xlabel('Dimension 1')
  plt.ylabel('Dimension 2')
  if DATATYPE == 'spatial': 
    plt.title('Spatial data') 
  if DATATYPE == 'temporal':  
    plt.title('Temporal data') 
  plt.savefig(os.path.join(fig_save_dir, 'tsne', 'tsne_fig_' + DATASET + '_rawData_'+ DATATYPE + '.png'))

if __name__ == '__main__':
    # os.environ['DATAPATH'] = "/home/fraser/Desktop/neurotac/SNN - Fraser/ICRA_2021/DataPath" #remove this
    # os.environ['PAPERPATH'] = "/home/fraser/Desktop/neurotac/SNN-Fraser/ICRA_2021/PaperPath" #Paper location
    main()

