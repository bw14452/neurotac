from brian2 import *
from collections import OrderedDict
from IPython.lib import kernel
from brian2 import SpikeGeneratorGroup
from collections import Counter
from hyperopt import fmin, tpe, hp
from sklearn import svm

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pickle
import time, os, json
import pandas

def main():
    global opt_accuracy
    global opt_nu_ee_pre
    global opt_nu_ee_post
    global opt_taum
    global opt_taue
    global opt_taui
    global opt_tau_pre_ee
    global opt_tau_post_1_ee
    global opt_tau_post_2_ee
    global opt_wmax_InF
    global opt_wmax_FE
    global opt_wmax_EI
    global opt_refractory_time
    global opt_IPSC
    
    opt_accuracy = []
    opt_nu_ee_pre = []
    opt_nu_ee_post = []
    opt_taum = []
    opt_taue = []
    opt_taui = []
    opt_tau_pre_ee = []
    opt_tau_post_1_ee = []
    opt_tau_post_2_ee = []
    opt_wmax_InF = []
    opt_wmax_FE = []
    opt_wmax_EI = []
    opt_refractory_time = []
    opt_IPSC = []
    
    #For Optimization
    # search_space = {
    #     'taum_opt': hp.uniform('taum_opt', 100, 120), 
    #     'taue_opt': hp.uniform('taue_opt', 0.8, 1), 
    #     'taui_opt': hp.uniform('taui_opt', 0.8, 1), 
            
    #     'nu_ee_pre_opt': hp.uniform('nu_ee_pre_opt', 0, 0.0005), 
    #     'nu_ee_post_opt': hp.uniform('nu_ee_post_opt', 0.05, 0.1),
    
    #     'tau_pre_ee_opt': hp.uniform('tau_pre_ee_opt', 8, 12), 
    #     'tau_post_1_ee_opt': hp.uniform('tau_post_1_ee_opt', 13, 17), 
    #     'tau_post_2_ee_opt': hp.uniform('tau_post_2_ee_opt', 18, 22), 
        
    #     'wmax_InF_opt': hp.uniform('wmax_InF_opt', 24, 26), 
    #     'wmax_FE_opt': hp.uniform('wmax_FE_opt', 0.04, 0.06), 
    #     'wmax_EI_opt': hp.uniform('wmax_EI_opt', 24, 26), 
        
    #     'refractory_time_opt': hp.uniform('refractory_time_opt', 1.7, 2.3), 
    #     'IPSC_opt': hp.uniform('IPSC_opt', 0.5, 0.7)
    # }   

    #For non-optimization

    search_space = {
        'taum_opt' : 110,
        'taue_opt' : 0.9,
        'taui_opt' : 0.9,
        'nu_ee_pre_opt' : 0.000025,
        'nu_ee_post_opt' : 0.075,
        'tau_pre_ee_opt' : 10,
        'tau_post_1_ee_opt' : 15,
        'tau_post_2_ee_opt' : 20,
        'wmax_InF_opt' : 25,
        'wmax_FE_opt' : 0.05,
        'wmax_EI_opt' : 25,
        'refractory_time_opt' : 2,
        'IPSC_opt' : 0.6
    } 



    # best = fmin(
    #     fn = k_fold,
    #     space = search_space,
    #     algo = tpe.suggest,
    #     max_evals = 1)
    # print(best)   

    best = k_fold(
        search_space)
#     print(best)
    
    opt_nu_ee_pre_sorted = sorted(opt_nu_ee_pre)
    opt_nu_ee_pre = np.array(opt_nu_ee_pre)
    opt_nu_ee_pre_index = np.argsort(opt_nu_ee_pre)
    opt_pre_accuracy_sorted = []
    for i in range(len(opt_nu_ee_pre_index)):
        opt_pre_accuracy_sorted.append(opt_accuracy[opt_nu_ee_pre_index[i]]) 
        
    opt_nu_ee_post_sorted = sorted(opt_nu_ee_post)
    opt_nu_ee_post = np.array(opt_nu_ee_post)
    opt_nu_ee_post_index = np.argsort(opt_nu_ee_post)
    opt_post_accuracy_sorted = []
    for i in range(len(opt_nu_ee_post_index)):
        opt_post_accuracy_sorted.append(opt_accuracy[opt_nu_ee_post_index[i]])     

    opt_taum_sorted = sorted(opt_taum)
    opt_taum = np.array(opt_taum)
    opt_taum_index = np.argsort(opt_taum)
    opt_taum_accuracy_sorted = []
    for i in range(len(opt_taum_index)):
        opt_taum_accuracy_sorted.append(opt_accuracy[opt_taum_index[i]]) 
        
    opt_taue_sorted = sorted(opt_taue)
    opt_taue = np.array(opt_taue)
    opt_taue_index = np.argsort(opt_taue)
    opt_taue_accuracy_sorted = []
    for i in range(len(opt_taue_index)):
        opt_taue_accuracy_sorted.append(opt_accuracy[opt_taue_index[i]]) 
        
    opt_taui_sorted = sorted(opt_taui)
    opt_taui = np.array(opt_taui)
    opt_taui_index = np.argsort(opt_taui)
    opt_taui_accuracy_sorted = []
    for i in range(len(opt_taui_index)):
        opt_taui_accuracy_sorted.append(opt_accuracy[opt_taui_index[i]])         
        
    opt_tau_pre_ee_sorted = sorted(opt_tau_pre_ee)
    opt_tau_pre_ee = np.array(opt_tau_pre_ee)
    opt_tau_pre_ee_index = np.argsort(opt_tau_pre_ee)
    opt_tau_pre_ee_accuracy_sorted = []
    for i in range(len(opt_tau_pre_ee_index)):
        opt_tau_pre_ee_accuracy_sorted.append(opt_accuracy[opt_tau_pre_ee_index[i]])         
        
    opt_tau_post_1_ee_sorted = sorted(opt_tau_post_1_ee)
    opt_tau_post_1_ee = np.array(opt_tau_post_1_ee)
    opt_tau_post_1_ee_index = np.argsort(opt_tau_post_1_ee)
    opt_tau_post_1_ee_accuracy_sorted = []
    for i in range(len(opt_tau_post_1_ee_index)):
        opt_tau_post_1_ee_accuracy_sorted.append(opt_accuracy[opt_tau_post_1_ee_index[i]])       
        
    opt_tau_post_2_ee_sorted = sorted(opt_tau_post_2_ee)
    opt_tau_post_2_ee = np.array(opt_tau_post_2_ee)
    opt_tau_post_2_ee_index = np.argsort(opt_tau_post_2_ee)
    opt_tau_post_2_ee_accuracy_sorted = []
    for i in range(len(opt_tau_post_2_ee_index)):
        opt_tau_post_2_ee_accuracy_sorted.append(opt_accuracy[opt_tau_post_2_ee_index[i]])         
        
    opt_wmax_InF_sorted = sorted(opt_wmax_InF)
    opt_wmax_InF = np.array(opt_wmax_InF)
    opt_wmax_InF_index = np.argsort(opt_wmax_InF)
    opt_wmax_InF_accuracy_sorted = []
    for i in range(len(opt_wmax_InF_index)):
        opt_wmax_InF_accuracy_sorted.append(opt_accuracy[opt_wmax_InF_index[i]]) 
        
    opt_wmax_FE_sorted = sorted(opt_wmax_FE)
    opt_wmax_FE = np.array(opt_wmax_FE)
    opt_wmax_FE_index = np.argsort(opt_wmax_FE)
    opt_wmax_FE_accuracy_sorted = []
    for i in range(len(opt_wmax_FE_index)):
        opt_wmax_FE_accuracy_sorted.append(opt_accuracy[opt_wmax_FE_index[i]])         
                
    opt_wmax_EI_sorted = sorted(opt_wmax_EI)
    opt_wmax_EI = np.array(opt_wmax_EI)
    opt_wmax_EI_index = np.argsort(opt_wmax_EI)
    opt_wmax_EI_accuracy_sorted = []
    for i in range(len(opt_wmax_EI_index)):
        opt_wmax_EI_accuracy_sorted.append(opt_accuracy[opt_wmax_EI_index[i]])         
        
    opt_refractory_time_sorted = sorted(opt_refractory_time)
    opt_refractory_time = np.array(opt_refractory_time)
    opt_refractory_time_index = np.argsort(opt_refractory_time)
    opt_refractory_time_accuracy_sorted = []
    for i in range(len(opt_refractory_time_index)):
        opt_refractory_time_accuracy_sorted.append(opt_accuracy[opt_refractory_time_index[i]])         
        
    opt_IPSC_sorted = sorted(opt_IPSC)
    opt_IPSC = np.array(opt_IPSC)
    opt_IPSC_index = np.argsort(opt_IPSC)
    opt_IPSC_accuracy_sorted = []
    for i in range(len(opt_IPSC_index)):
        opt_IPSC_accuracy_sorted.append(opt_accuracy[opt_IPSC_index[i]])         
        
        
#     plt.figure(1)
#     plt.plot(opt_nu_ee_pre_sorted, opt_pre_accuracy_sorted,'b-', label = 'accuracy')
#     plt.title('Accuracy changed with nu_ee_pre')
#     plt.xlabel('nu_ee_pre')
#     plt.ylabel('accuracy')
#     plt.legend()
#     plt.show()
    
#     plt.figure()
#     plt.plot(opt_nu_ee_post_sorted, opt_post_accuracy_sorted,'g-', label = 'accuracy')
#     plt.title('Accuracy changed with nu_ee_post')
#     plt.xlabel('nu_ee_post')
#     plt.ylabel('accuracy')
#     plt.legend()
#     plt.show()
    
#     plt.figure()
#     plt.plot(opt_taum_sorted, opt_taum_accuracy_sorted,'b-', label = 'accuracy')
#     plt.title('Accuracy changed with taum')
#     plt.xlabel('taum')
#     plt.ylabel('accuracy')
#     plt.legend()
#     plt.show()
    
#     plt.figure()
#     plt.plot(opt_taue_sorted, opt_taue_accuracy_sorted,'g-', label = 'accuracy')
#     plt.title('Accuracy changed with taue')
#     plt.xlabel('taue')
#     plt.ylabel('accuracy')
#     plt.legend()
#     plt.show()
    
#     plt.figure()
#     plt.plot(opt_taui_sorted, opt_taui_accuracy_sorted,'r-', label = 'accuracy')
#     plt.title('Accuracy changed with taui')
#     plt.xlabel('taui')
#     plt.ylabel('accuracy')
#     plt.legend()
#     plt.show()
    
#     plt.figure()
#     plt.plot(opt_tau_pre_ee_sorted, opt_tau_pre_ee_accuracy_sorted,'g-', label = 'accuracy')
#     plt.title('Accuracy changed with tau_pre_ee')
#     plt.xlabel('tau_pre_ee')
#     plt.ylabel('accuracy')
#     plt.legend()
#     plt.show()
    
#     plt.figure()
#     plt.plot(opt_tau_post_1_ee_sorted, opt_tau_post_1_ee_accuracy_sorted,'b-', label = 'accuracy')
#     plt.title('Accuracy changed with tau_post_1_ee')
#     plt.xlabel('tau_post_1_ee')
#     plt.ylabel('accuracy')
#     plt.legend()
#     plt.show()
    
#     plt.figure()
#     plt.plot(opt_tau_post_2_ee_sorted, opt_tau_post_2_ee_accuracy_sorted,'r-', label = 'accuracy')
#     plt.title('Accuracy changed with tau_post_2_ee')
#     plt.xlabel('tau_post_2_ee')
#     plt.ylabel('accuracy')
#     plt.legend()
#     plt.show()
    
#     plt.figure()
#     plt.plot(opt_wmax_InF_sorted, opt_wmax_InF_accuracy_sorted,'b-', label = 'accuracy')
#     plt.title('Accuracy changed with wmax_InF')
#     plt.xlabel('wmax_InF')
#     plt.ylabel('accuracy')
#     plt.legend()
#     plt.show()
    
#     plt.figure()
#     plt.plot(opt_wmax_FE_sorted, opt_wmax_FE_accuracy_sorted,'g-', label = 'accuracy')
#     plt.title('Accuracy changed with wmax_FE')
#     plt.xlabel('wmax_FE')
#     plt.ylabel('accuracy')
#     plt.legend()
#     plt.show()
    
#     plt.figure()
#     plt.plot(opt_wmax_EI_sorted, opt_wmax_EI_accuracy_sorted,'r-', label = 'accuracy')
#     plt.title('Accuracy changed with wmax_EI')
#     plt.xlabel('wmax_EI')
#     plt.ylabel('accuracy')
#     plt.legend()
#     plt.show()
    
#     plt.figure()
#     plt.plot(opt_refractory_time_sorted, opt_refractory_time_accuracy_sorted,'g-', label = 'accuracy')
#     plt.title('Accuracy changed with refractory_time')
#     plt.xlabel('refractory_time')
#     plt.ylabel('accuracy')
#     plt.legend()
#     plt.show()
    
#     plt.figure()
#     plt.plot(opt_IPSC_sorted, opt_IPSC_accuracy_sorted,'b-', label = 'accuracy')
#     plt.title('Accuracy changed with IPSC')
#     plt.xlabel('IPSC')
#     plt.ylabel('accuracy')
#     plt.legend()
#     plt.show()
    

    
def k_fold(params_opt):
    global opt_accuracy
    global opt_nu_ee_pre
    global opt_nu_ee_post
    global opt_taum
    global opt_taue
    global opt_taui
    global opt_tau_pre_ee
    global opt_tau_post_1_ee
    global opt_tau_post_2_ee
    global opt_wmax_InF
    global opt_wmax_FE
    global opt_wmax_EI
    global opt_refractory_time
    global opt_IPSC
    
    taum_opt = params_opt['taum_opt']
    taue_opt = params_opt['taue_opt']
    taui_opt = params_opt['taui_opt']
    
    nu_ee_pre_opt = params_opt['nu_ee_pre_opt']    
    nu_ee_post_opt = params_opt['nu_ee_post_opt']
    
    tau_pre_ee_opt = params_opt['tau_pre_ee_opt']
    tau_post_1_ee_opt = params_opt['tau_post_1_ee_opt']
    tau_post_2_ee_opt = params_opt['tau_post_2_ee_opt']
    
    wmax_InF_opt= params_opt['wmax_InF_opt']
    wmax_FE_opt = params_opt['wmax_FE_opt']
    wmax_EI_opt = params_opt['wmax_EI_opt']
    
    refractory_time_opt = params_opt['refractory_time_opt']
    IPSC_opt = params_opt['IPSC_opt']
    
    
    
#    trials = np.arange(10)
#     learning_trials = np.array([trials[0], trials[2], trials[4]])
#     k_accuracy = []
#     # for i in range(len(learning_trials)):
#     #     test_trials = np.array(learning_trials[i])
#     #     train_trials = np.delete(learning_trials, i)
#     #     k_accuracy.append(edge_taps(learning_trials, test_trials, train_trials, params_opt))
#     for i in range (1):
#         test_trials = np.array(learning_trials[i])
#         train_trials = np.delete(learning_trials, i)
#         k_accuracy.append(edge_taps(learning_trials, test_trials, train_trials, params_opt))
    
    # k_accuracy = []
    # numtrials = np.arange(20)
    # test_trials = np.array([numtrials[4], numtrials[8], numtrials[12], numtrials[16]])
    # train_trials = [0, 1, 2, 3, 5, 6, 7, 9, 10, 11, 13, 14, 15, 17, 18, 19]
    # k_accuracy.append(edge_taps(numtrials, test_trials, train_trials, params_opt))


    k_accuracy = []
    numtrials = [20]
    n_poses = [18]
    train_trials = [16]
    test_trials = [4]
    k_accuracy.append(edge_taps(numtrials, test_trials, train_trials, params_opt))
        
#     for i in range (len(numtrials)):
#         if i == 4 or i == 8 or i == 12 or i == 16:
#             np.append()
            

    
    print('there should be accuracy here but currently commented out line 364')
    # accuracy = np.mean(k_accuracy)  
    # opt_accuracy.append(accuracy) 
    # opt_nu_ee_pre.append(nu_ee_pre_opt)
    # opt_nu_ee_post.append(nu_ee_post_opt)
    # opt_taum.append(taum_opt)
    # opt_taue.append(taue_opt)
    # opt_taui.append(taui_opt)
    # opt_tau_pre_ee.append(tau_pre_ee_opt)
    # opt_tau_post_1_ee.append(tau_post_1_ee_opt)
    # opt_tau_post_2_ee.append(tau_post_2_ee_opt)
    # opt_wmax_InF.append(wmax_InF_opt)
    # opt_wmax_FE.append(wmax_FE_opt)
    # opt_wmax_EI.append(wmax_EI_opt)
    # opt_refractory_time.append(refractory_time_opt)
    # opt_IPSC.append(IPSC_opt)     
    
    
    
    #return accuracy
    
    
    
    
    
    
    
def edge_taps(learning_trials, test_trials, train_trials, params_opt):

    
    taum_opt = params_opt['taum_opt']
    taue_opt = params_opt['taue_opt']
    taui_opt = params_opt['taui_opt']
    
    nu_ee_pre_opt = params_opt['nu_ee_pre_opt']    
    nu_ee_post_opt = params_opt['nu_ee_post_opt']
    
    tau_pre_ee_opt = params_opt['tau_pre_ee_opt']
    tau_post_1_ee_opt = params_opt['tau_post_1_ee_opt']
    tau_post_2_ee_opt = params_opt['tau_post_2_ee_opt']
    
    wmax_InF_opt= params_opt['wmax_InF_opt']
    wmax_FE_opt = params_opt['wmax_FE_opt']
    wmax_EI_opt = params_opt['wmax_EI_opt']
    
    refractory_time_opt = params_opt['refractory_time_opt']
    IPSC_opt = params_opt['IPSC_opt']
    
    start_scope()
    test_mode = False
    
    # Data directory
    data_dir_name = 'collect_orientation_tap'    
    home_dir = os.path.join(os.environ['DATAPATH'])
    data_dir = os.path.join(home_dir, data_dir_name)
    fig_save_dir = os.path.join(os.environ['PAPERPATH'])
    if not os.path.exists(fig_save_dir):
        os.makedirs(fig_save_dir)
#    test_fig_save_dir = os.path.join(os.environ['PAPERPATH'],'2020_SNNs','test_figures', data_dir_name)
#    if not os.path.exists(test_fig_save_dir):
#        os.makedirs(test_fig_save_dir)

    # Load metadata #
    with open(data_dir + "/meta.json", "r") as read_file:
        meta = json.load(read_file)
    #For full data
    #n_trials = meta['num_trials']
    #n_poses = meta['num_poses']
    #train_trials = 16
    #test_trials = 4
    #For reduced data (debugging later code)
    n_trials = 2
    n_poses = 1
    train_trials = 1
    test_trials = 1    
    
########################################## PARAMETERS ########################################################
    
    # # Simulation parameters
    runtime = 1000
    WINDOW_WIDTH = 128
    WINDOW_HEIGHT = 128
    n_inputs = WINDOW_WIDTH*WINDOW_HEIGHT
    n_e = 100   # number of neurons in the E layer
    n_i = n_e   # number of neurons in the I layer
    
    # # Max weights for each layer
    wmax_InF= wmax_InF_opt
    wmax_FE = wmax_FE_opt
    wmax_EI = wmax_EI_opt   
    
    # # Neuron equation parameters
    taum = taum_opt*ms
    Ee = 0*mV             # potential
    vt = -50*mV           # threshold potential
    vr = -60*mV           # resting potential 
    El = -70*mV           # potential
    taue = taue_opt*ms
    taui = taui_opt*ms

    refractory_time = refractory_time_opt*ms
    IPSC = IPSC_opt             # inhibitory weights
    
    v_rest_e = -65. * mV 
    v_rest_i = -60. * mV 
    v_reset_e = -65. * mV
    v_reset_i = -45. * mV
    v_thresh_e = -52. * mV
    v_thresh_i = -40. * mV
    refrac_e = 5*ms
    refrac_i = 2*ms
    
    tau_pre_ee = 9.11*ms         # 20*ms
    tau_post_1_ee = 16.34*ms   # 20*ms
    tau_post_2_ee = 18.51*ms   # 20*ms
    
    # # Learning rates
    nu_ee_pre = nu_ee_pre_opt               # learning rate for decreasing weights 0.0001
    nu_ee_post = nu_ee_post_opt                # learning rate for increasing weights 0.01
    
    # # Neuron equations
    eqs_e = '''
        dv/dt = ((v_rest_e - v) + (I_synE+I_synI) / nS) / taum   : volt
        I_synE = ge * nS *         -v                            : amp
        I_synI = gi * nS * (-100.*mV-v)                          : amp
        dge/dt = -ge/(taue)                                      : 1
        dgi/dt = -gi/(taui)                                      : 1
        x: meter
        y: meter
        '''
    if test_mode:
        eqs_e += '\n  theta      :volt'
        scr_e = 'v = v_reset_e; timer = 0*ms'
    else:
        tau_theta = 1e7*ms
        theta_plus_e = 0.05*mV
        scr_e = 'v = v_reset_e; theta += theta_plus_e; timer = 0*ms'
        eqs_e += '\n  dtheta/dt = -theta / (tau_theta)  : volt'
    eqs_e += '\n  dtimer/dt = 100.0  : second'
    
    eqs_i = '''
        dv/dt = ((v_rest_i - v) + (I_synE+I_synI) / nS) / (10*ms)  : volt
        I_synE = ge * nS *         -v                              : amp
        I_synI = gi * nS * (-85.*mV-v)                             : amp
        dge/dt = -ge/(1.0*ms)                                      : 1
        dgi/dt = -gi/(2.0*ms)                                      : 1
        '''
    eqs_stdp_ee = '''
        w                                       : 1
        post2before                             : 1
        dpre/dt   =   -pre/(tau_pre_ee)         : 1 (event-driven)
        dpost1/dt  = -post1/(tau_post_1_ee)     : 1 (event-driven)
        dpost2/dt  = -post2/(tau_post_2_ee)     : 1 (event-driven)
        '''
    eqs_stdp_pre_ee = 'ge+=w;pre = 1.; w -= nu_ee_pre * post1'
    eqs_stdp_post_ee = 'post2before = post2; w += nu_ee_post * pre * post2before; post1 = 1.; post2 = 1.'
        

################################################## LAYERS ####################################################
    
# Network structure : 4 layers
# In: input layer 128x128
# F: filter layer 32x32
# E: excitatory layer 100
# I: inhibitory layer 100
    
    print('Begin')
    
    reduction_factor = 4 
    grid_dist = reduction_factor*meter
    In = SpikeGeneratorGroup(n_inputs, []*n_inputs, []*n_inputs*ms)

#     Filter Layers
    F_0 = NeuronGroup(n_inputs//(reduction_factor**2), eqs_e, threshold='v > v_thresh_e', reset= scr_e, refractory=2*ms)
    F_0.v=-70*mV
    F_0.x = (1.0 + F_0.i//(128//reduction_factor))*grid_dist
    F_0.y = '(1.0 + (i%(128//reduction_factor)))*grid_dist'
    F_1 = NeuronGroup(n_inputs//(reduction_factor**2), eqs_e, threshold='v > v_thresh_e', reset= scr_e, refractory=2*ms)
    F_1.v=-70*mV
    F_1.x = (1.0 + F_1.i//(128//reduction_factor))*grid_dist
    F_1.y = '(1.0 + (i%(128//reduction_factor)))*grid_dist'
    F_2 = NeuronGroup(n_inputs//(reduction_factor**2), eqs_e, threshold='v > v_thresh_e', reset= scr_e, refractory=2*ms)
    F_2.v=-70*mV
    F_2.x = (1.0 + F_2.i//(128//reduction_factor))*grid_dist
    F_2.y = '(1.0 + (i%(128//reduction_factor)))*grid_dist'
    F_3 = NeuronGroup(n_inputs//(reduction_factor**2), eqs_e, threshold='v > v_thresh_e', reset= scr_e, refractory=2*ms)
    F_3.v=-70*mV
    F_3.x = (1.0 + F_3.i//(128//reduction_factor))*grid_dist
    F_3.y = '(1.0 + (i%(128//reduction_factor)))*grid_dist'
    F_4 = NeuronGroup(n_inputs//(reduction_factor**2), eqs_e, threshold='v > v_thresh_e', reset= scr_e, refractory=2*ms)
    F_4.v=-70*mV
    F_4.x = (1.0 + F_4.i//(128//reduction_factor))*grid_dist
    F_4.y = '(1.0 + (i%(128//reduction_factor)))*grid_dist'
       
    E = NeuronGroup(n_e, eqs_e, threshold= 'v>v_thresh_e', refractory= refrac_e, reset= scr_e)
    E.v = v_rest_e - 25. * mV
    I = NeuronGroup(n_i, eqs_i, threshold= 'v>v_thresh_i', refractory= refrac_i, reset= 'v=v_reset_i')
    I.v = v_rest_i - 40. * mV
    

############################################## CONNECTIONS ###################################################
  
#     # I-F Connections

    synapses_InF0 = Synapses(In, F_0,model = 
        eqs_stdp_ee,
        on_pre= eqs_stdp_pre_ee ,
        on_post=eqs_stdp_post_ee)

    synapses_InF0.connect('sqrt((i//128 - (x_post / meter))**2 + (i%128 - (y_post / meter))**2) < reduction_factor', skip_if_invalid=True)
#    synapses_InF.connect('j=k for k in range(x_post-reduction_factor//2,x_post+reduction_factor//2) if //2', skip_if_invalid=True)
    synapses_InF0.w = 'rand()* 0.75* wmax_InF'
    
    synapses_InF1 = Synapses(In, F_1,model = 
        eqs_stdp_ee,
        on_pre= eqs_stdp_pre_ee ,
        on_post=eqs_stdp_post_ee)

    synapses_InF1.connect('sqrt((i//128 - (x_post / meter))**2 + (i%128 - (y_post / meter))**2) < reduction_factor', skip_if_invalid=True)
#    synapses_InF.connect('j=k for k in range(x_post-reduction_factor//2,x_post+reduction_factor//2) if //2', skip_if_invalid=True)
    synapses_InF1.w = 'rand()* 0.75* wmax_InF'
    
    synapses_InF2 = Synapses(In, F_2,model = 
        eqs_stdp_ee,
        on_pre= eqs_stdp_pre_ee ,
        on_post=eqs_stdp_post_ee)

    synapses_InF2.connect('sqrt((i//128 - (x_post / meter))**2 + (i%128 - (y_post / meter))**2) < reduction_factor', skip_if_invalid=True)
#    synapses_InF.connect('j=k for k in range(x_post-reduction_factor//2,x_post+reduction_factor//2) if //2', skip_if_invalid=True)
    synapses_InF2.w = 'rand()* 0.75* wmax_InF'

    synapses_InF3 = Synapses(In, F_3,model = 
        eqs_stdp_ee,
        on_pre= eqs_stdp_pre_ee ,
        on_post=eqs_stdp_post_ee)

    synapses_InF3.connect('sqrt((i//128 - (x_post / meter))**2 + (i%128 - (y_post / meter))**2) < reduction_factor', skip_if_invalid=True)
#    synapses_InF.connect('j=k for k in range(x_post-reduction_factor//2,x_post+reduction_factor//2) if //2', skip_if_invalid=True)
    synapses_InF3.w = 'rand()* 0.75* wmax_InF'

    synapses_InF4 = Synapses(In, F_4,model = 
        eqs_stdp_ee,
        on_pre= eqs_stdp_pre_ee ,
        on_post=eqs_stdp_post_ee)

    synapses_InF4.connect('sqrt((i//128 - (x_post / meter))**2 + (i%128 - (y_post / meter))**2) < reduction_factor', skip_if_invalid=True)
#    synapses_InF.connect('j=k for k in range(x_post-reduction_factor//2,x_post+reduction_factor//2) if //2', skip_if_invalid=True)
    synapses_InF4.w = 'rand()* 0.75* wmax_InF'

#test print for weights#
    print('Weights prior to begin')
    print(synapses_InF0.w)
    print(synapses_InF1.w)
    print(synapses_InF2.w)
    print(synapses_InF3.w)
    print(synapses_InF4.w)


# Archived multimodel structure
#    #Neuron compartmentation
#     filterclustering = Cylinder(n=5, diameter=32*um, length=32*um)
#     #filterclustering.L = Cylinder(length=1*um, diameter=1*um, n=1)
#     #filterclustering.R = Cylinder(length=1*um, diameter=1*um, n=1)
#     F = SpatialNeuron(morphology=filterclustering, model=eqs_e, threshold='v > v_thresh_e', reset= scr_e, refractory=2*ms) 
    
#     synapses_InF = Synapses (In, filtercluster, model = eqs_stdp_ee,
#         on_pre= eqs_stdp_pre_ee ,
#         on_post=eqs_stdp_post_ee)
    
#     synapses_InF.connect('sqrt((i//128 - (x_post / meter))**2 + (i%128 - (y_post / meter))**2) < reduction_factor', skip_if_invalid=True)
#     synapses_InF.w = '0.75 * wmax_InF'
         

    synapses_F0E = Synapses(F_0, E, model = 
        eqs_stdp_ee,
        on_pre=eqs_stdp_pre_ee,
        on_post=eqs_stdp_post_ee)
    synapses_F0E.connect()
    synapses_F0E.w = 'rand()* wmax_FE'
    
    synapses_F1E = Synapses(F_1, E, model = 
        eqs_stdp_ee,
        on_pre=eqs_stdp_pre_ee,
        on_post=eqs_stdp_post_ee)
    synapses_F1E.connect()
    synapses_F1E.w = 'rand()* wmax_FE'
    
    synapses_F2E = Synapses(F_2, E, model = 
        eqs_stdp_ee,
        on_pre=eqs_stdp_pre_ee,
        on_post=eqs_stdp_post_ee)
    synapses_F2E.connect()
    synapses_F2E.w = 'rand()* wmax_FE'
    
    synapses_F3E = Synapses(F_3, E, model = 
        eqs_stdp_ee,
        on_pre=eqs_stdp_pre_ee,
        on_post=eqs_stdp_post_ee)
    synapses_F3E.connect()
    synapses_F3E.w = 'rand()* wmax_FE'
    
    synapses_F4E = Synapses(F_4, E, model = 
        eqs_stdp_ee,
        on_pre=eqs_stdp_pre_ee,
        on_post=eqs_stdp_post_ee)
    synapses_F4E.connect()
    synapses_F4E.w = 'rand()* wmax_FE'

    
    
    
    
    # E-I Connections
    synapses_EI = Synapses(E, I,
        '''w : 1''',
        on_pre='''ge+= wmax_EI''')
    synapses_EI.connect(j='i')
    synapses_EI.w = 'wmax_EI'
                         
    # I-E Connections
    synapses_IE = Synapses(I, E,
        '''w : 1''',
        on_pre='''gi += IPSC''')
    synapses_IE.connect(condition='i != j')

    # Monitors
    In_mon = SpikeMonitor(In)
    F_0_mon = SpikeMonitor(F_0)
    F_1_mon = SpikeMonitor(F_1)
    F_2_mon = SpikeMonitor(F_2)
    F_3_mon = SpikeMonitor(F_3)
    F_4_mon = SpikeMonitor(F_4)
    E_mon = SpikeMonitor(E)
    I_mon = SpikeMonitor(I)
    weights_InF0_mon = StateMonitor(synapses_InF0,'w', record=range(0,1000,10))
    weights_F0E_mon = StateMonitor(synapses_F0E,'w', record=synapses_F0E[:,0])
    weights_InF1_mon = StateMonitor(synapses_InF1,'w', record=range(0,1000,10))
    weights_F1E_mon = StateMonitor(synapses_F1E,'w', record=synapses_F1E[:,0])
    weights_InF2_mon = StateMonitor(synapses_InF2,'w', record=range(0,1000,10))
    weights_F2E_mon = StateMonitor(synapses_F2E,'w', record=synapses_F2E[:,0])
    weights_InF3_mon = StateMonitor(synapses_InF3,'w', record=range(0,1000,10))
    weights_F3E_mon = StateMonitor(synapses_F3E,'w', record=synapses_F3E[:,0])
    weights_InF4_mon = StateMonitor(synapses_InF4,'w', record=range(0,1000,10))
    weights_F4E_mon = StateMonitor(synapses_F4E,'w', record=synapses_F4E[:,0])
 #   v_E_mon = StateMonitor(E,'v', record=True)
    
    # Store network state
    #net = Network(In, F_0, F_1, F_2, F_3, F_4, E, I, synapses_InF0, synapses_InF1, synapses_InF2, synapses_InF3, synapses_InF4, synapses_F0E, synapses_F1E, synapses_F2E, synapses_F3E, synapses_F4E, synapses_EI, synapses_IE, In_mon, F_0_mon, F_1_mon, F_2_mon, F_3_mon, F_4_mon, E_mon, I_mon)
    net = Network(In, F_0, F_1, F_2, F_3, F_4, E, I, synapses_InF0, synapses_InF1, synapses_InF2, synapses_InF3, synapses_InF4, synapses_F0E, synapses_F1E, synapses_F2E, synapses_F3E, synapses_F4E, synapses_EI, synapses_IE, In_mon, F_0_mon, F_1_mon, F_2_mon, F_3_mon, F_4_mon, E_mon, I_mon, weights_InF0_mon, weights_InF1_mon, weights_InF2_mon, weights_InF3_mon, weights_InF4_mon, weights_F0E_mon, weights_F1E_mon, weights_F2E_mon, weights_F3E_mon, weights_F4E_mon)
    net.store('initialized')
    init_weights = init_weights_FE(synapses_F0E, n_e, reduction_factor)
#    fig_init_weights(synapses_InF, synapses_FE, n_e, reduction_factor,fig_save_dir)

############################################### TRAINING ####################################################
    
    # training loop #
    print('#### Training Start ####')
    net_flag = 0
    train_loop = 0
    x_list = []
    y_list = []
#    cla = pd.DataFrame({'train_loop_'+str(train_loop): zeros(n_e)})
    
    for pose_idx in range(n_poses):                 
        for trial_idx in range(train_trials):
#            train_loop = train_loop+1
            print('#### Pose ' + str(pose_idx+1) + ' / ' + str(n_poses) + ' trial ' + str(trial_idx+1) + ' / ' + str(n_trials) + ' ####')

            # Set spikes for this pose #
            spikes_i = []
            spikes_t = []

            # Load data #
            filename = os.path.join(data_dir, 'data_pose_' + str(pose_idx) + '_trial_' + str(trial_idx)+'.pickle')
            infile = open(filename,'rb')
            data = pickle.load(infile)       
            infile.close()
            data = remove_duplicates(data)
            
            # Generate spikes for network #
            for i in range(WINDOW_WIDTH):
                for j in range(WINDOW_HEIGHT):
                    if data[i][j] != []:
                        for spike in data[i][j]:
                            spikes_i.append(i*WINDOW_WIDTH+j)
                            spikes_t.append(spike+1000*trial_idx+1000*train_trials*pose_idx)
            In.set_spikes(spikes_i,spikes_t*ms)
            # Make connectivity figures #
#            if pose_idx == 0 and trial_idx ==0:
                
            # Run network # 
            net.run(runtime*ms)




            #Figures #
            fig_spikes(In_mon, F_0_mon, F_1_mon, F_2_mon, F_3_mon, F_4_mon, E_mon,I_mon, n_e, pose_idx, trial_idx, n_poses, train_trials, n_inputs, reduction_factor, runtime, fig_save_dir, net_flag)
#            cla['train_loop_'+str(train_loop)] = E_mon.count         
            fig_heatmap(In_mon,F_0_mon, F_1_mon, F_2_mon, F_3_mon, F_4_mon, pose_idx,trial_idx,n_trials, reduction_factor,grid_dist, runtime, fig_save_dir)
#            fig_E(E_mon, v_E_mon, weights_F0E_mon, weights_F1E_mon, weights_F2E_mon, weights_F3E_mon, weights_F4E_mon, pose_idx,trial_idx,n_trials,n_inputs, reduction_factor, runtime, fig_save_dir)        
#            fig_dopamine(E_mon, dopamine_mon, weights_E_mon, pose_idx, trial_idx, meta['num_poses'])
#            fig_STDP(S, weights_mon, gmax,pose_idx,trial_idx)
#    fig_connectivity(synapses_FE, synapses_EI)
#    fig_final_weights(synapses_InF, synapses_FE, n_e, reduction_factor,fig_save_dir)
#    fig_final_weights_change(init_weights, synapses_FE, n_e, reduction_factor, fig_save_dir)
   
            e_layer_outputs = [[] for i in range (n_e)]
            for i in range (n_e):
                if len(E_mon.spike_trains()[i]) > 0:
                    e_layer_outputs[i] = E_mon.spike_trains()[i][0]
                elif len(E_mon.spike_trains()[i]) == 0:
                    e_layer_outputs[i] = 0

            # #Take weights from monitors...
            # updatedWeightInF0 = weights_InF0_mon.get_states('w')
            # updatedWeightInF1 = weights_InF1_mon.get_states('w')
            # updatedWeightInF2 = weights_InF2_mon.get_states('w')
            # updatedWeightInF3 = weights_InF3_mon.get_states('w')
            # updatedWeightInF4 = weights_InF4_mon.get_states('w')
            # updatedWeightF0E = weights_F0E_mon.get_states('w')
            # updatedWeightF1E = weights_F1E_mon.get_states('w')
            # updatedWeightF2E = weights_F2E_mon.get_states('w')
            # updatedWeightF3E = weights_F3E_mon.get_states('w')
            # updatedWeightF4E = weights_F4E_mon.get_states('w')
            # #...and push them into new 
            ##
            #test print for weights#
            print('Weights after run')
            print(synapses_InF0.w)
            print(synapses_InF1.w)
            print(synapses_InF2.w)
            print(synapses_InF3.w)
            print(synapses_InF4.w)
            ##
            # synapses_InF0.w = updatedWeightInF0['w'][1000]
            # synapses_InF1.w = updatedWeightInF1['w']
            # synapses_InF2.w = updatedWeightInF2['w']
            # synapses_InF3.w = updatedWeightInF3['w']
            # synapses_InF4.w = updatedWeightInF4['w']
            #net = Network(In, F_0, F_1, F_2, F_3, F_4, E, I, synapses_InF0, synapses_InF1, synapses_InF2, synapses_InF3, synapses_InF4, synapses_F0E, synapses_F1E, synapses_F2E, synapses_F3E, synapses_F4E, synapses_EI, synapses_IE, In_mon, F_0_mon, F_1_mon, F_2_mon, F_3_mon, F_4_mon, E_mon, I_mon, weights_InF0_mon, weights_InF1_mon, weights_InF2_mon, weights_InF3_mon, weights_InF4_mon, weights_F0E_mon, weights_F1E_mon, weights_F2E_mon, weights_F3E_mon, weights_F4E_mon)
            #net.store('initialized')

            # e_layer_outputs = [[] for i in range(n_e)]    
            # poses = [int(i//(len(train_trials)*runtime*ms)) for i in E_mon.t]
            # for neuron in range(len(E_mon.i)):
            #     e_layer_outputs[E_mon.i[neuron]].append(poses[neuron])
            #e_layer_classes = [Counter(i).most_common(1)[0][0] if len(i)>0 else -1 for i in e_layer_outputs]

            x_list.append(e_layer_outputs)
            y_list.append(pose_idx)

#    print(e_layer_outputs)
            print(e_layer_outputs)
            print(weights_InF0_mon)
    net.store('after_training')
    
    #Temporary for tracking weight changes
    plot(weights_InF0_mon.t / ms, weights_InF0_mon[synapses_InF0[0]].w / nS)
    plt.savefig('temporary_spike_holder')
    plt.close()
#    neu = pd.DataFrame(zeros(n_e))
#    for pose_idx in range(n_poses):
#        cla['spike_count_pose_'+str(pose_idx+1)] = cla['train_loop_'+str((pose_idx+1)*train_trials)]-cla['train_loop_'+str(pose_idx*train_trials)]
#        max_value = cla['spike_count_pose_'+str(pose_idx+1)].max(axis=0)
#        max_index = cla.groupby('spike_count_pose_'+str(pose_idx+1)).apply(lambda x: tuple(x.index) if len(x.index)>1 else None).dropna()
#        neu['pose_'+str(pose_idx+1)+'_spike_neurons'] = pd.DataFrame(max_index.loc[max_value])
    print('#### Training Done ####')     
    
    
################################################# TESTING ###################################################


    print('#### Testing Start ####')
    test_loop = 0
#    cla['test_loop_'+str(test_loop)] = cla['train_loop_'+str(train_loop)]
    net_flag = 1
    classified_poses = []
    target_poses = []
    x_list_test = []
    y_list_test = []
    
    for pose_idx in range(n_poses):               
        for trial_idx in range(test_trials): 
            test_loop = test_loop+1
            net.restore('after_training')
            print('#### Pose ' + str(pose_idx+1) + ' / ' + str(n_poses) + ' trial ' + str(trial_idx+1+(train_trials)) + ' / ' + str(n_trials) + ' ####')               
            spikes_i = []
            spikes_t = []
            filename = os.path.join(data_dir, 'data_pose_' + str(pose_idx) + '_trial_' + str(trial_idx)+'.pickle')
            infile = open(filename,'rb')
            data = pickle.load(infile)                    
            data = remove_duplicates(data)
            for i in range(WINDOW_WIDTH):
                for j in range(WINDOW_HEIGHT):
                    if data[i][j] != []:
                        for spike in data[i][j]:
                            spikes_i.append(i*WINDOW_WIDTH+j)
                            spikes_t.append(spike+1000*train_trials+1000*train_trials*(n_poses-1))
            In.set_spikes(spikes_i,spikes_t*ms)
            net.run(runtime*ms)
             
            fig_spikes(In_mon, F_0_mon, F_1_mon, F_2_mon, F_3_mon, F_4_mon, E_mon,I_mon, n_e, pose_idx, trial_idx, n_poses, train_trials, n_inputs, reduction_factor, runtime, fig_save_dir, net_flag)
            fig_heatmap(In_mon,F_0_mon, F_1_mon, F_2_mon, F_3_mon, F_4_mon, pose_idx,trial_idx,n_trials, reduction_factor,grid_dist, runtime, fig_save_dir)

            # spike_poses = []
            # for spikes in range(len(E_mon.t)):
            #     if E_mon.t[spikes] > (runtime*len(train_trials)*n_poses)*ms and E_mon.t[spikes] <= (runtime*len(train_trials)*n_poses+runtime)*ms:
            #         spike_poses.append(e_layer_classes[E_mon.i[spikes]])

            e_layer_outputs = [[] for i in range (n_e)]
            for i in range (n_e):
                if len(E_mon.spike_trains()[i]) > 0:
                    e_layer_outputs[i] = E_mon.spike_trains()[i][0]
                elif len(E_mon.spike_trains()[i]) == 0:
                    e_layer_outputs[i] = 0
 


            # e_layer_outputs = [[] for i in range(n_e)]    
            # poses = [int(i//(len(train_trials)*runtime*ms)) for i in E_mon.t]
            # for neuron in range(len(E_mon.i)):
            #     e_layer_outputs[E_mon.i[neuron]].append(poses[neuron])
            # e_layer_classes = [Counter(i).most_common(1)[0][0] if len(i)>0 else -1 for i in e_layer_outputs]
                    
#            classified_pose = Counter(spike_poses).most_common(1)[0][0]

            print(e_layer_outputs)

            x_list_test.append(e_layer_outputs)
            y_list_test.append(pose_idx)

######################################################## -- Classifier --########################################################
    #X = np.asarray(spike_poses)
    #Y = np.asarray(e_layer_classes)

    # clf = svm.SVC(decision_function_shape='ovr')
    # clf.fit(x_list, y_list)
    # dec = clf.decision_function([[1]])
    # dec.shape[1] 
    
    
    
    # X_train = spike_poses
    # X_test = e_layer_classes

    # classifier_params = {'C': 1.0, 'gamma': 'auto'}
    # train_mean = np.mean(X_train, axis=0)
    # train_std = np.std(X_train, axis=0)
    # X_train -= train_mean
    # X_test -= train_mean
    # X_train /= (train_std + 1e-5)
    # X_test /= (train_std + 1e-5)
    # svm = classifier(X_train, y_train, X_test, y_test, classifier_params, classifier_type='SVM')
    # train_score, test_score = svm.run_classiffier()   
    # print('Train Score: ' + str(train_score))
    # print('Test Score: ' + str(test_score))


############################################################################################################   old classifier
    #         counter = Counter(spike_poses)
    #         classified_pose = counter.most_common(1)[0][0] if counter else -1
    #         classified_poses.append(classified_pose)
    #         target_poses.append(pose_idx)



    # accuracy = (len(classified_poses) - np.count_nonzero([a - b for a, b in zip(classified_poses, target_poses)]))/len(classified_poses)
    # print('Target poses: ' + str(target_poses))
    # print('Classified poses: ' + str(classified_poses))
    # print('Accuracy: ' + str(accuracy))           
############################################################################################################    



#            cla['test_loop_'+str(test_loop)] = E_mon.count
            
#    for pose_idx in range(n_poses):
#        cla['test_spike_count_pose_'+str(pose_idx+1)] = cla['test_loop_'+str((pose_idx+1)*test_trials)]-cla['train_loop_'+str(train_loop)]
#        test_max_value = cla['test_spike_count_pose_'+str(pose_idx+1)].max(axis=0)
#        test_max_index = cla.groupby('test_spike_count_pose_'+str(pose_idx+1)).apply(lambda x: tuple(x.index) if len(x.index)>1 else None).dropna()
#        neu['test_pose_'+str(pose_idx+1)+'_spike_neurons'] = pd.DataFrame(test_max_index.loc[test_max_value])    
    print('#### Testing Done ####')
    
    
    
    #return accuracy    
    
    
################################################# FUNCTIONS ###################################################



def remove_duplicates(data):
    for i in range(len(data)):
        for j in range(len(data[i])): 
            data[i][j] = list(OrderedDict.fromkeys(data[i][j]))
    return data

def fig_connectivity(S, fig_save_dir):
    fig_dir = fig_save_dir
    if not os.path.exists(fig_dir):
        os.makedirs(fig_dir)
#    Ns = len(S.source)
#    Nt = len(S.target)
    Ns = 1000
    Nt = len(S.target)
    figure(figsize=(5, 4))
    plot(zeros(Ns), arange(Ns), 'ok', ms=2)
    plot(ones(Nt), arange(Nt)*100, 'ok', ms=2)
#    for i, j in zip(S.i[:62], S.j[:62]):
    plot([0, 1], [S.i, S.j], '-k')
    xticks([0, 1], ['F','E'])
    ylabel('Neuron index')
    xlim(-0.1, 1.1)
    ylim(-1, max(Ns, Nt)-1)
    title('Network connectivity')
    plt.savefig(fig_dir + '/connectivity')
    plt.close()
         


def fig_spikes(spikes_In, spikes_F_0, F_1_mon, F_2_mon, F_3_mon, F_4_mon, spikes_E, spikes_I, n_e, pose_idx, trial_idx, n_poses, n_trials, n_inputs, reduction_factor, runtime, fig_save_dir, net_flag):
    if net_flag == 0:
        fig_dir = os.path.join(fig_save_dir, 'training_spikes')
        if not os.path.exists(fig_dir):
            os.makedirs(fig_dir)
        #start_time = pose_idx*n_trials*runtime+trial_idx*runtime
        start_time = 1
    if net_flag == 1:
        fig_dir = os.path.join(fig_save_dir, 'testing_spikes')
        if not os.path.exists(fig_dir):
            os.makedirs(fig_dir)
        #start_time = (n_poses-1)*n_trials*runtime+n_trials*runtime
        start_time = 1        
    end_time = start_time + runtime
    print(start_time)
    figure(figsize=(18,10))
    subplot(331)
    plot(spikes_In.t/ms, spikes_In.i, '.k',markersize=0.5)
    xlabel('Time (ms)')
    ylabel('Neuron index')
    xlim([start_time, end_time])
    ylim([0, n_inputs])
    title('Input spikes')
    subplot(332)
    plot(spikes_F_0.t/ms, spikes_F_0.i, '.k',markersize=0.5)
    xlabel('Time(ms)')
    ylabel('Neuron index')
    xlim([start_time, end_time])
    ylim([0, n_inputs//(reduction_factor**2)])
    title('F_0 layer spikes')
    subplot(333)
    plot(F_1_mon.t/ms, F_1_mon.i, '.k',markersize=0.5)
    xlabel('Time(ms)')
    ylabel('Neuron index')
    xlim([start_time, end_time])
    ylim([0, n_inputs//(reduction_factor**2)])
    title('F_1 layer spikes')
    subplot(334)
    plot(F_2_mon.t/ms, F_2_mon.i, '.k',markersize=0.5)
    xlabel('Time(ms)')
    ylabel('Neuron index')
    xlim([start_time, end_time])
    ylim([0, n_inputs//(reduction_factor**2)])
    title('F_2 layer spikes')
    subplot(335)
    plot(F_3_mon.t/ms, F_3_mon.i, '.k',markersize=0.5)
    xlabel('Time(ms)')
    ylabel('Neuron index')
    xlim([start_time, end_time])
    ylim([0, n_inputs//(reduction_factor**2)])
    title('F_3 layer spikes')
    subplot(336)
    plot(F_4_mon.t/ms, F_4_mon.i, '.k',markersize=0.5)
    xlabel('Time(ms)')
    ylabel('Neuron index')
    xlim([start_time, end_time])
    ylim([0, n_inputs//(reduction_factor**2)])
    title('F_4 layer spikes')
    subplot(337)
    plot(spikes_E.t/ms, spikes_E.i, '.k')
    xlabel('Time(ms)')
    ylabel('Neuron index')
    xlim([start_time, end_time])
    ylim([0, n_e])
    title('E layer spikes')
    # subplot(258)
    # plot(spikes_I.t/ms, spikes_I.i, '.k')
    # xlabel('Time(ms)')
    # ylabel('Neuron index')
    # xlim([start_time, end_time])
    # ylim([0, n_e])
    # title('I layer spikes')
   # plt.show()
    plt.tight_layout(pad=3.0)
    plt.savefig(fig_dir + '/spikes_pose_'+str(pose_idx)+'_trial_'+str(trial_idx))
    plt.close()

def fig_E(E_mon, v_E_mon, weights_F0E_mon, weights_F1E_mon, weights_F2E_mon, weights_F3E_mon, weights_F4E_mon,pose_idx,trial_idx,n_trials,n_inputs, reduction_factor, runtime, fig_save_dir):
    fig_dir = os.path.join(fig_save_dir, 'E_layer')
    if not os.path.exists(fig_dir):
        os.makedirs(fig_dir)
    start_time = pose_idx*n_trials*runtime+trial_idx*runtime
    end_time = start_time + runtime
    
    figure(figsize=(12,4))
    subplot(251)
    plot(E_mon.t/ms, E_mon.i, '.k')
    xlabel('Time(ms)')
    ylabel('Neuron index')
    xlim([start_time, start_time+1000])
    ylim([-0.1, 18.1])
    yticks(np.arange(0, 18, step=1))
    title('E spikes')
    subplot(252)
    plot(v_E_mon.t/ms, v_E_mon.v.T, 'k')
    xlabel('Time (ms)')
    ylabel('Neuron index')
    xlim([start_time, start_time+1000])
#    ylim([0, n_inputs])
    title('E voltage')
    subplot(253)
    col_totals = [sum(x) for x in zip(*weights_F0E_mon.w)]
    plot(weights_F0E_mon.t/ms, col_totals, 'k')
    xlabel('Time(ms)')
    ylabel('Neuron index')
    xlim([start_time, start_time+1000])
#    ylim([0, n_inputs//(reduction_factor**2)])
    title('E0 weights')
    subplot(255)
    col_totals = [sum(x) for x in zip(*weights_F1E_mon.w)]
    plot(weights_F1E_mon.t/ms, col_totals, 'k')
    xlabel('Time(ms)')
    ylabel('Neuron index')
    xlim([start_time, start_time+1000])
#    ylim([0, n_inputs//(reduction_factor**2)])
    title('E1 weights')
    subplot(256)
    col_totals = [sum(x) for x in zip(*weights_F2E_mon.w)]
    plot(weights_F2E_mon.t/ms, col_totals, 'k')
    xlabel('Time(ms)')
    ylabel('Neuron index')
    xlim([start_time, start_time+1000])
#    ylim([0, n_inputs//(reduction_factor**2)])
    title('E2 weights')
    subplot(257)
    col_totals = [sum(x) for x in zip(*weights_F3E_mon.w)]
    plot(weights_F3E_mon.t/ms, col_totals, 'k')
    xlabel('Time(ms)')
    ylabel('Neuron index')
    xlim([start_time, start_time+1000])
#    ylim([0, n_inputs//(reduction_factor**2)])
    title('E3 weights')
    subplot(258)
    col_totals = [sum(x) for x in zip(*weights_F4E_mon.w)]
    plot(weights_F4E_mon.t/ms, col_totals, 'k')
    xlabel('Time(ms)')
    ylabel('Neuron index')
    xlim([start_time, start_time+1000])
#    ylim([0, n_inputs//(reduction_factor**2)])
    title('E4 weights')
#    plt.show()
    plt.savefig(fig_dir + '/spikes_pose_'+str(pose_idx)+'_trial_'+str(trial_idx))
    plt.close()
##########################################################################################################################################################################################  
def fig_heatmap(spikes_I, spikes_F0, spikes_F1, spikes_F2, spikes_F3, spikes_F4, pose_idx, trial_idx, n_trials, reduction_factor, grid_dist, runtime, fig_save_dir):
    fig_dir = os.path.join(fig_save_dir,'heatmap')
    if not os.path.exists(fig_dir):
        os.makedirs(fig_dir)
    start_time = pose_idx*n_trials*runtime+trial_idx*runtime
    end_time = start_time + runtime
    filter_frame = 128//reduction_factor
    
    figure(figsize=(18,10))
    subplot(331)
    x = []
    y = []
    for i in range(len(spikes_I.i)): 
        if spikes_I.t[i]/ms > start_time and spikes_I.t[i]/ms < end_time:
            x.append(spikes_I.i[i]//128)
            y.append(spikes_I.i[i]%128)
    plt.hist2d(x, y, 128, range=np.array([(0, 128), (0, 128)]))
    xlabel('x pixel')
    ylabel('y pixel')
    xlim([0, 128])
    ylim([0, 128])
    title('Input spikes heatmap')
    plt.colorbar()
    
    subplot(332)
    x = []
    y = []
    for i in range(len(spikes_F0.i)): 
        if spikes_F0.t[i]/ms > start_time and spikes_F0.t[i]/ms < end_time: 
            x.append(spikes_F0.i[i]//filter_frame)
            y.append(spikes_F0.i[i]%filter_frame)
    plt.hist2d(x, y, filter_frame, range=np.array([(0, filter_frame ), (0, filter_frame )]))
    xlim([0, filter_frame])
    ylim([0, filter_frame])
    xlabel('x pixel')
    ylabel('y pixel')
    title('Filter layer 0 spikes heatmap')
     
    subplot(333)
    x = []
    y = []
    for i in range(len(spikes_F1.i)): 
        if spikes_F1.t[i]/ms > start_time and spikes_F1.t[i]/ms < end_time: 
            x.append(spikes_F1.i[i]//filter_frame)
            y.append(spikes_F1.i[i]%filter_frame)
    plt.hist2d(x, y, filter_frame, range=np.array([(0, filter_frame ), (0, filter_frame )]))
    xlim([0, filter_frame])
    ylim([0, filter_frame])
    xlabel('x pixel')
    ylabel('y pixel')
    title('Filter layer 1 spikes heatmap')

    subplot(334)
    x = []
    y = []
    for i in range(len(spikes_F2.i)): 
        if spikes_F2.t[i]/ms > start_time and spikes_F2.t[i]/ms < end_time: 
            x.append(spikes_F2.i[i]//filter_frame)
            y.append(spikes_F2.i[i]%filter_frame)
    plt.hist2d(x, y, filter_frame, range=np.array([(0, filter_frame ), (0, filter_frame )]))
    xlim([0, filter_frame])
    ylim([0, filter_frame])
    xlabel('x pixel')
    ylabel('y pixel')
    title('Filter layer 2 spikes heatmap')

    subplot(335)
    x = []
    y = []
    for i in range(len(spikes_F3.i)): 
        if spikes_F3.t[i]/ms > start_time and spikes_F3.t[i]/ms < end_time: 
            x.append(spikes_F3.i[i]//filter_frame)
            y.append(spikes_F3.i[i]%filter_frame)
    plt.hist2d(x, y, filter_frame, range=np.array([(0, filter_frame ), (0, filter_frame )]))
    xlim([0, filter_frame])
    ylim([0, filter_frame])
    xlabel('x pixel')
    ylabel('y pixel')
    title('Filter layer 3 spikes heatmap')

    subplot(336)
    x = []
    y = []
    for i in range(len(spikes_F4.i)): 
        if spikes_F4.t[i]/ms > start_time and spikes_F4.t[i]/ms < end_time: 
            x.append(spikes_F4.i[i]//filter_frame)
            y.append(spikes_F4.i[i]%filter_frame)
    plt.hist2d(x, y, filter_frame, range=np.array([(0, filter_frame ), (0, filter_frame )]))
    xlim([0, filter_frame])
    ylim([0, filter_frame])
    xlabel('x pixel')
    ylabel('y pixel')
    title('Filter layer 4 spikes heatmap')

    plt.colorbar()
#    plt.show()
    plt.tight_layout(pad=3.0)
    plt.savefig(fig_dir + '/heatmap_pose_'+str(pose_idx)+'_trial_'+str(trial_idx))
    plt.close()
##########################################################################################################################################################################################  

def fig_final_weights(synapses_InF, synapses_FE, n_e, reduction_factor, fig_save_dir):
    fig_dir = os.path.join(fig_save_dir, 'weights', 'weights_InF')
    if not os.path.exists(fig_dir):
        os.makedirs(fig_dir)
    figure(figsize=(5, 4))
    
    filter_frame = 128//reduction_factor
#    dx, dy = 1, 1
#    y, x = np.mgrid[slice(0, filter_frame, dy),
#        slice(0, filter_frame, dx)]
#    z = [[0]*filter_frame for x in range(filter_frame)]
    
#    for i in range(filter_frame**2):
#        weights = synapses_InF.w[:,i]
#        for i in range(len(weights)):
#            weight = sum(weights)
#            row = i//filter_frame
#            column = i%filter_frame
#            z[row][column] = weight

#    plt.pcolormesh(x,y,z)
#    plt.colorbar()
#    xlabel('Neuron x')
#    ylabel('Neuron y')
#    title('F layer final weights')
#    tight_layout()
#    plt.savefig(fig_dir + '/weights_InF')
#    plt.close()

    fig_dir = os.path.join(fig_save_dir, 'weights', 'weights_FE')
    if not os.path.exists(fig_dir):
        os.makedirs(fig_dir)
    figure(figsize=(5, 4))

    dx, dy = 1, 1
    y, x = np.mgrid[slice(0, filter_frame, dy),
         slice(0, filter_frame, dx)]
    z = [[0]*filter_frame for x in range(filter_frame)]
    
    for j in range(0,n_e):
        weights = synapses_FE.w[:,j]        
        for i in range(len(weights)):
            weight = weights[i]
            row = i//filter_frame
            column = i%filter_frame
            z[row][column] = weight

        plt.pcolormesh(x,y,z)
        plt.colorbar()
        xlabel('Synapse x')
        ylabel('Synapse y')
        title('E layer neuron '+ str(j) + ' synapse weights')
        tight_layout()
        plt.savefig(fig_dir + '/weights_FE_neuron_'+str(j))
        plt.close()

def fig_final_weights_change(init_weights_FE, synapses_FE, n_e, reduction_factor, fig_save_dir):
    
    filter_frame = 128//reduction_factor
    fig_dir = os.path.join(fig_save_dir, 'weights', 'weights_FE')
    if not os.path.exists(fig_dir):
        os.makedirs(fig_dir)
    figure(figsize=(5, 4))

    dx, dy = 1, 1
    y, x = np.mgrid[slice(0, filter_frame, dy),
         slice(0, filter_frame, dx)]
    z = [[0]*filter_frame for x in range(filter_frame)]
    
    for j in range(0,n_e,2):
        weights = synapses_FE.w[:,j]
        for i in range(len(weights)):
            weight = weights[i]
            row = i//filter_frame
            column = i%filter_frame
            z[row][column] = weight - init_weights_FE[row][column]

        plt.pcolormesh(x,y,z)
        plt.colorbar()
        xlabel('Synapse x')
        ylabel('Synapse y')
        title('E layer neuron '+ str(j) + ' synapse weight change')
        tight_layout()
        plt.savefig(fig_dir + '/weights_FE_neuron_'+str(j)+'_change')
        plt.close()
        
def init_weights_FE(synapses_FE, n_e, reduction_factor):
    filter_frame = 128//reduction_factor
    dx, dy = 1, 1
    y, x = np.mgrid[slice(0, filter_frame, dy),
         slice(0, filter_frame, dx)]
    z = [[0]*filter_frame for x in range(filter_frame)]
    
    for j in range(0,n_e):
        weights = synapses_FE.w[:,j]
        for i in range(len(weights)):
            weight = weights[i]
            row = i//filter_frame
            column = i%filter_frame
            z[row][column] = weight
    return z


def fig_init_weights(synapses_InF, synapses_FE, n_e, reduction_factor, fig_save_dir):
    fig_dir = os.path.join(fig_save_dir, 'weights', 'weights_InF')
    if not os.path.exists(fig_dir):
        os.makedirs(fig_dir)
    figure(figsize=(5, 4))
    
    filter_frame = 128//reduction_factor
#    dx, dy = 1, 1
#    y, x = np.mgrid[slice(0, filter_frame, dy),
#         slice(0, filter_frame, dx)]
#    z = [[0]*filter_frame for x in range(filter_frame)]
    
#    for i in range(filter_frame**2):
#        weights = synapses_InF.w[:,i]
#        weight = sum(weights)
#        row = i//filter_frame
#        column = i%filter_frame
#        z[row][column] = weight

#    plt.pcolormesh(x,y,z)
#    plt.colorbar()
#    xlabel('Neuron x')
#    ylabel('Neuron y')
#    title('F layer initial weights')
#    tight_layout()
#    plt.savefig(fig_dir + '/weights_InF_init')
#    plt.close()

    fig_dir = os.path.join(fig_save_dir, 'weights', 'weights_FE')
    if not os.path.exists(fig_dir):
        os.makedirs(fig_dir)
    figure(figsize=(5, 4))

    dx, dy = 1, 1
    y, x = np.mgrid[slice(0, filter_frame, dy),
         slice(0, filter_frame, dx)]
    z = [[0]*filter_frame for x in range(filter_frame)]
    
    for j in range(0,n_e,2):
        weights = synapses_FE.w[:,j]
        for i in range(len(weights)):
            weight = weights[i]
            row = i//filter_frame
            column = i%filter_frame
            z[row][column] = weight

        plt.pcolormesh(x,y,z)
        plt.colorbar()
        xlabel('Synapse x')
        ylabel('Synapse y')
        title('E layer neuron '+ str(j) + ' synapse weights')
        tight_layout()
        plt.savefig(fig_dir + '/weights_FE_neuron_'+str(j)+'_init')
        plt.close()


if __name__ == '__main__':
    #if at home
    #os.environ['DATAPATH'] = "C:\\Users\\Frase\\OneDrive\\Documents\\DataPath\\TacTip_NM\\edgeTap_Dobot" 
    #os.environ['PAPERPATH'] = "C:\\Users\\Frase\\OneDrive\\Documents\\PaperPath\\Data_generation" 
    #if on macbook
    #os.environ['DATAPATH'] = "/Users/frasersmacbook/Desktop/neurotac/SNN - Fraser/ICRA_2021/DataPath/"
    #os.environ['PAPERPATH'] = "/Users/frasersmacbook/Desktop/neurotac/SNN - Fraser/ICRA_2021/PaperPath/"
    #if at work 
    os.environ['DATAPATH'] = "/home/fraser/Desktop/neurotac/SNN - Fraser/ICRA_2021/DataPath" #remove this
    os.environ['PAPERPATH'] = "/home/fraser/Desktop/neurotac/SNN - Fraser/ICRA_2021/PaperPath" #Paper location
   
    main()