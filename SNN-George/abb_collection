# -*- coding: utf-8 -*-
import os
import time
import json
import numpy as np
import pickle
import threading

from cri.robot import SyncRobot, AsyncRobot
from cri.controller import ABBController
from core.sensor.tactile_sensor_neuro import NeuroTac_DAVIS240


class NeuroCollection():
  def __init__(self, robot_tcp=[0, 0, 75, 0, 0, 0], base_frame=[0, 0, 0, 0, 0, 0], home_pose=[400, 0, 300, 180, 0, 180], work_frame=[452, -125, 53, 180, 0, 180], linear_speed=10, angular_speed=20, tap_move=[[0, 0, 6, 0, 0, 0], [50, 0, 6, 0, 0, 0], [50, 0, 0, 0, 0, 0]], obj_poses=[[0, y, 0, 0, 0, 0] for y in range(0, 450, 50)], n_trials=11):
    # Place all the position variables into class code
    self.robot_tcp = robot_tcp
    self.base_frame = base_frame
    self.home_pose = home_pose
    self.work_frame = work_frame
    self.linear_speed = linear_speed
    self.angular_speed = angular_speed
    self.tap_move = tap_move
    self.obj_poses = obj_poses
    self.n_trials = n_trials
    print("Robot Controller initialised")

  def print_meta(self):
    print(f"robot_tcp = {self.robot_tcp}")
    print(f"base_frame = {self.base_frame}")
    print(f"home_pose = {self.home_pose}")
    print(f"work_frame = {self.work_frame}")
    print(f"linear_speed = {self.linear_speed}")
    print(f"angular_speed = {self.angular_speed}")
    print(f"tap_move = {self.tap_move}")
    print(f"obj_poses = {self.obj_poses}")
    print(f"n_trials = {self.n_trials}")

  def __make_robot(self):
    return AsyncRobot(SyncRobot(ABBController(ip='192.168.125.1')))

  def __make_sensor(self):
    return NeuroTac_DAVIS240(port=33859)

  def add_obj_poses(self, pose):
    self.obj_poses.append(pose)
    print(f"Appended pose: {pose}")

  def move_home(self):
    with self.__make_robot() as robot:
      robot.tcp = self.robot_tcp
      # Move to home position
      print("Moving to home position ...")
      robot.coord_frame = self.base_frame
      robot.linear_speed = 50
      robot.move_linear(self.home_pose)
      print("Arrived at home position")

  def move_work_frame(self):
    with self.__make_robot() as robot:
      robot.tcp = self.robot_tcp
      # Move to origin of work frame
      print("Moving to origin of work frame ...")
      robot.coord_frame = self.work_frame
      robot.linear_speed = 50
      robot.move_linear((0, 0, 0, 0, 0, 0))
      robot.linear_speed = self.linear_speed

  def single_drag(self,target_index):
    # Function to perform a single drag from the arm and return the neuroTac data
    with self.__make_robot() as robot, self.__make_sensor() as sensor:

      robot.tcp = self.robot_tcp

      # move to home position
      print("Moving to home position ...")
      robot.coord_frame = self.base_frame
      robot.linear_speed = 75
      robot.move_linear(self.home_pose)

      # Display robot info
      print("Robot info: {}".format(robot.info))

      # Display initial pose in work frame
      print("Initial pose in work frame: {}".format(robot.pose))

      # Move to origin of work frame
      print("Moving to origin of work frame ...")
      robot.coord_frame = self.work_frame
      robot.move_linear((0, 0, 0, 0, 0, 0))

      sensor.reset_variables()

      print("Moving to single starting position")
      robot.move_linear(self.obj_poses[target_index])

      # Set TCP, linear speed,  angular speed and coordinate frame
      robot.linear_speed = self.linear_speed
      robot.angular_speed = self.angular_speed

      # Start sensor recording
      #print("Starting recording")
      sensor.start_logging()
      t = threading.Thread(target=sensor.get_pixel_events, args=())
      t.start()

      robot.coord_frame = self.base_frame
      robot.coord_frame = robot.pose
      robot.move_linear(self.tap_move[0])
      robot.move_linear(self.tap_move[1])
      robot.move_linear(self.tap_move[2])
      robot.coord_frame = self.work_frame

      # Stop sensor recording
      sensor.stop_logging()
      t.join()

      # Collate proper timestamp values in ms.
      sensor.value_cleanup()
      print("Data recorded")

      # Move to home position
      print("Moving to home position ...")
      robot.coord_frame = self.base_frame
      robot.linear_speed = 75
      robot.move_linear(self.home_pose)

    print("Returning sensor data from collection")
    return sensor.events_on

  def single_drag_save(self, target_index, OUTPUT_PATH):
      # Function to perform a single drag from the arm and return the neuroTac data
    with self.__make_robot() as robot, self.__make_sensor() as sensor:

      robot.tcp = self.robot_tcp

      # move to home position
      print("Moving to home position ...")
      robot.coord_frame = self.base_frame
      robot.linear_speed = 75
      robot.move_linear(self.home_pose)

      # Display robot info
      print("Robot info: {}".format(robot.info))

      # Display initial pose in work frame
      print("Initial pose in work frame: {}".format(robot.pose))

      # Move to origin of work frame
      print("Moving to origin of work frame ...")
      robot.coord_frame = self.work_frame
      robot.move_linear((0, 0, 0, 0, 0, 0))

      sensor.reset_variables()

      print("Moving to single starting position")
      robot.move_linear(self.obj_poses[target_index])

      # Set TCP, linear speed,  angular speed and coordinate frame
      robot.linear_speed = self.linear_speed
      robot.angular_speed = self.angular_speed

      # Start sensor recording
      #print("Starting recording")
      sensor.start_logging()
      t = threading.Thread(target=sensor.get_pixel_events, args=())
      t.start()

      robot.coord_frame = self.base_frame
      robot.coord_frame = robot.pose
      robot.move_linear(self.tap_move[0])
      robot.move_linear(self.tap_move[1])
      robot.move_linear(self.tap_move[2])
      robot.coord_frame = self.work_frame

      # Stop sensor recording
      sensor.stop_logging()
      t.join()

      # Collate proper timestamp values in ms.
      sensor.value_cleanup()

      # Save data
      pickle_out = open(
          f"{OUTPUT_PATH}/Artificial Dataset {target_index}Texture No. 0.pickle", 'wb')
      pickle.dump(sensor.events_on, pickle_out)
      pickle_out.close()
      print("Data recorded")

      # Move to home position
      print("Moving to home position ...")
      robot.coord_frame = self.base_frame
      robot.linear_speed = 75
      robot.move_linear(self.home_pose)

    print("Returning sensor data from collection")
    return sensor.events_on

  def no_reset_drag(self,target_index):
    # Function to perform a single drag from the arm and not return to home position before returning neuroTac data
    # Does not move to home position before or after drag collection
    with self.__make_robot() as robot, self.__make_sensor() as sensor:

      robot.tcp = self.robot_tcp

      sensor.reset_variables()

      # Move quickly between samples
      robot.linear_speed = 75
      robot.angular_speed = 75

      robot.coord_frame = self.work_frame
      print("Moving to single starting position")
      robot.move_linear(self.obj_poses[target_index])

      # Set TCP, linear speed,  angular speed and coordinate frame
      robot.linear_speed = self.linear_speed
      robot.angular_speed = self.angular_speed

      # Start sensor recording
      #print("Starting recording")
      sensor.start_logging()
      t = threading.Thread(target=sensor.get_pixel_events, args=())
      t.start()

      robot.coord_frame = self.base_frame
      robot.coord_frame = robot.pose
      robot.move_linear(self.tap_move[0])
      robot.move_linear(self.tap_move[1])
      robot.move_linear(self.tap_move[2])
      robot.coord_frame = self.work_frame

      # Stop sensor recording
      sensor.stop_logging()
      t.join()

      # Collate proper timestamp values in ms.
      sensor.value_cleanup()
      print("Data recorded")
      print("Drag complete")

      robot.linear_speed = 75
      robot.angular_speed = 75

    print("Returning sensor data from collection")
    return sensor.events_on


  # TODO: Fix this completely
  def collect_dataset(self, OUTPUT_PATH):
    with self.__make_robot() as robot, self.__make_sensor() as sensor:

      robot.tcp = self.robot_tcp

      # move to home position
      print("Moving to home position ...")
      robot.coord_frame = self.base_frame
      robot.linear_speed = 50
      robot.move_linear(self.home_pose)

      # Display robot info
      print("Robot info: {}".format(robot.info))

      # Display initial pose in work frame
      print("Initial pose in work frame: {}".format(robot.pose))

      for trial_idx in range(self.n_trials):

        # Move to origin of work frame
        print("Moving to origin of work frame ...")
        robot.coord_frame = self.work_frame
        robot.move_linear((0, 0, 0, 0, 0, 0))
        pose_idx = 0

        for pose_idx in range(len(self.obj_poses)):

          sensor.reset_variables()

          # Set speed for between drags
          robot.linear_speed = 75
          robot.angular_speed = 75

          robot.move_linear(self.obj_poses[pose_idx])
          # time.sleep(1)

          # Set speed for drag
          robot.linear_speed = self.linear_speed
          robot.angular_speed = self.angular_speed

          # Start sensor recording
          sensor.start_logging()
          t = threading.Thread(target=sensor.get_pixel_events, args=())
          t.start()

          # Tap
          print("Trial " + str(trial_idx+1) + "/" + str(self.n_trials) +
                " Pose " + str(pose_idx+1) + "/" + str(len(self.obj_poses)))
          robot.coord_frame = self.base_frame
          robot.coord_frame = robot.pose
          robot.move_linear(self.tap_move[0])
          robot.move_linear(self.tap_move[1])
          robot.move_linear(self.tap_move[2])
          robot.coord_frame = self.work_frame

          # Stop sensor recording
          sensor.stop_logging()
          t.join()

          # Collate proper timestamp values in ms.
          sensor.value_cleanup()

          # Save data
          pickle_out = open(f"{OUTPUT_PATH}/Artificial Dataset {trial_idx}Texture No. {pose_idx}.pickle", 'wb')
          pickle.dump(sensor.events_on, pickle_out)
          pickle_out.close()
          print("Data recorded")

          #pose_idx += 1

            # # # Unwind sensor
            # print("Unwinding")
            # robot.move_linear([sum(x) for x in zip(
            #     robot.pose, [0, 0, 0, 0, 0, -170])])

        # # Move to home position
        # print("Moving to home position ...")
        # robot.coord_frame = self.base_frame
        # robot.linear_speed = 50
        # robot.move_linear(self.home_pose)

  # def run_collection(self, meta_path):
  #   # Make and save metadata
  #   meta_file = os.path.join(meta_path, 'meta.json')
  #   meta = self.make_meta(meta_file)
  #   os.makedirs(os.path.dirname(meta_file))
  #   with open(meta_file, 'w') as f:
  #       json.dump(meta, f)

  #   # Collect data
  #   self.collect_data(meta_path, **meta)


if __name__ == '__main__':
  # Create RobotController object and run its collection method
  abb = NeuroCollection(work_frame=[510, -175, 53, 180, 0, 180], obj_poses=[[0, y, 0, 0, 0, 0] for y in range(0, 450, 50)], linear_speed=10)
  #abb.print_meta()
  print(np.unique(abb.single_drag(2))) #z=53 normally
