from asyncio import events
from turtle import shape
import numpy as np
import pickle
import os
import glob
import time
from sklearn.model_selection import train_test_split
#from personal_lib import functions as pl

# TO DO: 1) Can a string be an input? Does it have to be converted?
def convert_to_aer(DATA, ON_OFF = 1):
    """ Function to create convert pickled neuroTac data into aer format

    Arguments
    ----------
    DATA:   list of lists (nested_list)
                    Nested list data output by the neuroTac (often pickled)
    ON_OFF: Integer
                    Input to state whether data contains OFF events. 0 = OFF events included. Default = 1

    Returns
    -------
    DATA_AER: numpy array
                    Array containing spike data in aer format
    """
    
    DATA_AER = []   # List of all pixel events to be output from function
    temp_list = [] # Create a temporary list to contain information for all events

    # Cycle through each nested list and check if empty
    # Check row
    for x in range(DATA.shape[0]):
        # Check column
        for y in range(DATA.shape[1]):
            # Check if pixel (x,y) is empty
            if DATA[x,y]:
                # Cycle through all events in this pixel
                for spike in DATA[x,y]:
                    temp_list.append([x, y, ON_OFF, spike])  # Currently we only input ON events

    # Order by timestamp
    sorted_list = sorted(temp_list, key=lambda x: x[3])
    
    # Convert to 40 bit binary number
    for element in sorted_list:

        x_bin = '{0:08b}'.format(element[0])  # Convert x value to 8bit
        y_bin = '{0:08b}'.format(element[1])
        ON_bin = str(element[2])
        timestamp_bin = '{0:23b}'.format((element[3]*100))  # Multiply timestamp by 100 to covert from ms to us
 
        # Create a 40bit binary string for this event
        b40_string = timestamp_bin + ON_bin + y_bin + x_bin

        # Append this string to the output list
        DATA_AER.append(b40_string)

    DATA_AER = np.array(DATA_AER)

    return DATA_AER


def create_dataset(PATH, num_labels, split_ratio=0.2):
    """ Function to create a test and train dataset from a folder of pickled neuroTac data

    Arguments
    ----------
    PATH:           string
                        Input string to show path to dataset folder
    num_labels:     int
                        Input integer to give number of classes in dataset
    split_ratio:    float (default=0.2)
                        Float input to determine the ratio at which the test and training data should be split

    Returns
    -------
    train_dataset: numpy array
                    Pickled array containing the training dataset. First column gives label, second is a list of events in aer format
    test_dataset: numpy array
                    Pickled array containing the testing dataset. First column gives label, second is a list of events in aer format
    """

    # Find number of pickled files in directory
    num_samples = len(glob.glob1(PATH,"*.pickle"))

    # Create empty numpy array for dataset
    # Data types are specified to improve processing times
    # Labels column is specified as a int8
    # Events column is specified as holding an array of events
    labels_empty = np.zeros(shape=(num_samples,1), dtype=np.int8)
    events_empty = np.zeros(shape=(num_samples,1), dtype=np.ndarray)
    dataset = np.hstack([labels_empty, events_empty])

    #t = time.time()
    # Loop through all recorded samples to create large dataset array
    for label in range(num_labels):
        # Assuming a uniform number of samples per label filter through each sample
        for sample in range(int(num_samples/num_labels)-1):
            
             # Import test data
            FILE_NAME = f"Artificial Dataset {sample}Texture No. {label}.pickle"
            DATA_PATH = "/home/farscope2/Documents/PhD/First_Year_Project/SpikingNetsTexture/datasets/TacTip_NM/Reduced/" + FILE_NAME

            # Import the sample
            spike_times = np.load(DATA_PATH, allow_pickle=True)

            events = convert_to_aer(spike_times)

            # Append this samples data to the dataset array
            dataset[label*sample] = [label, events]
            
            # Print status every 5 samples
            if sample % 5 == 0:
                print(f"Label: {label}, Sample: {sample}")

    #print(time.time() - t)
    # Split data into training and testing splits based on split ratio
    label_train, label_test, data_train, data_test = train_test_split(dataset[0], dataset[1], test_size=split_ratio, shuffle=True)

    # Recombine parts into new test and train arrays
    train_dataset = np.hstack([label_train, data_train])
    test_dataset = np.hstack([label_test, data_test])

    # Save this dataset to two new folders
    os.makedirs(PATH + "train")
    os.makedirs(PATH + "test")

    pickle_out = open(os.path.join(PATH, "train/training_dataset.pickle"), 'wb')
    pickle.dump(train_dataset, pickle_out)
    pickle_out.close()  

    pickle_out = open(os.path.join(PATH, "test/testing_dataset.pickle"), 'wb')
    pickle.dump(test_dataset, pickle_out)
    pickle_out.close()  




if __name__ == '__main__':

    path = "/home/farscope2/Documents/PhD/First_Year_Project/SpikingNetsTexture/datasets/TacTip_NM/Reduced/"
    create_dataset(path, 11)